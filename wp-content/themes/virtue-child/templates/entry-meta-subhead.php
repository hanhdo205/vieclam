<div class="subhead">
    <span class="postauthortop author vcard">
    <i class="icon-user"></i> <?php echo __('by', 'virtue');?>  <span itemprop="author"><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" class="fn" rel="author"><?php echo get_the_author() ?></a></span> |</span>
    <?php $post_category = get_the_category($post->ID); if ( $post_category==true ) { ?>  
    <span class="postedintop"><i class="icon-folder-open"></i> <?php _e('posted in:', 'virtue'); ?> <?php the_category(', ') ?></span> <?php }?>

    <?php $cat = get_the_job_category_bytax($post->ID); if ( $cat==true ) { ?>  
    <span class="postedintop"><i class="icon-folder-open"></i> <?php _e('posted in:', 'virtue'); ?> <?php echo $cat; ?></span> <?php }?>

    <?php $re_cat = get_the_resume_category_bytax($post->ID); if ( $re_cat==true ) { ?>  
    <span class="postedintop"><i class="icon-folder-open"></i> <?php _e('posted in:', 'virtue'); ?> <?php echo $re_cat; ?></span> <?php }?>

    <span class="kad-hidepostedin">|</span>
    <?php if ((get_post_type( $post->ID )=='resume')||(get_post_type( $post->ID )=='job_listing')) : ?>
    <span class="postcommentscount">
    <i class="icon-eye-open"></i> <?php echo getPostViews($post->ID); ?>
    </span>
    <?php else : ?>
    <span class="postcommentscount">
    <i class="icon-comments-alt"></i> <?php comments_number( '0', '1', '%' ); ?>
    </span>
<?php endif; ?>
</div>