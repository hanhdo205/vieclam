<?php global $virtue; ?>
<header class="banner headerclass" role="banner">
<?php 
//detect mobile agent
if ( wp_is_mobile() ) {
    // any mobile platform
    echo "<style>#fixed {position: relative;}</style>";
} else echo '<script src="http://code.jquery.com/jquery-latest.js"></script>';
?>

<?php if (kadence_display_topbar()) : ?>
  <section id="topbar" class="topclass large">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6 topbarmenu kad-topbar-left">
          <div class="topbarmenu clearfix">

<!-- Topbar menu -->
<div id="topbar" class="nojq nojs" role="navigation">
	<div class="quicklinks">
		<?php if ( !is_user_logged_in()) { ?>
		<ul id="menu-topbar-navigation" class="login sf-menu sf-js-enabled"  <?php if (!get_option('users_can_register')) { ?> style="width:150px;"<?php } ?>>
	    	<!-- Login / Register -->			
			<li><a id="login" class="ab-item" aria-haspopup="true" href="#"><?php _e('Log In'); ?></a>
				<div class="ab-sub-wrapper">
					<ul class="ab-submenu">
					<?php echo login_form();?>
					</ul>
				</div>
			</li> 
			<li><a id="dangky" href="<?php echo site_url()?>/dang-ky-tai-khoan/"><?php _e('Register'); ?></a></li>
			<li><a id="jobalerts" href="<?php echo site_url()?>/thong-bao-viec-lam-moi/?action=add_alert">Nhận việc làm mới</a></li>
		</ul>
		<?php } else { ?>
		<ul id="menu-topbar-navigation" class="login sf-menu sf-js-enabled"  <?php if (!get_option('users_can_register')) { ?> style="width:150px;"<?php } ?>>
	    	<!-- Login / Register -->			
			<li><a id="register" class="ab-item" aria-haspopup="true" href="#"><?php $user_info = get_userdata( get_current_user_id() );
			printf( __( 'Howdy, %1$s' ), $user_info->display_name );?><?php echo get_wp_user_avatar(get_current_user_id(), 18);?></a>
			<div class="ab-sub-wrapper">
			<ul class="chevon ab-submenu">
				<li>
				<span class="content">
					<p><?php $last_login = get_last_login( get_current_user_id() );
					if ($last_login) echo "Đăng nhập lúc: ".$last_login;?>
					<span class="avatar"><?php echo get_wp_user_avatar(get_current_user_id(), 64);?></span>
					</p>
					<p style="margin:10px 0 0;"><a class="ab-item" href="<?php echo site_url()?>/thong-tin-dang-nhap/">Tài khoản<?php //_e('Edit My Profile'); ?></a></p>
					<p style="margin:0;"><a class="ab-item" href="<?php echo wp_logout_url( get_permalink() ); ?>">Thoát<?php //_e('Log out'); ?></a></p>
				</span>
				</li>
				
				<?php if (has_nav_menu('slide_down')) : 
				wp_nav_menu(array('theme_location' => 'slide_down', 'menu_class' => 'ab-submenu', ));
				endif;?>
					
			</ul>
			</div>
			</li> 			 	
		</ul>
		<?php } ?>

		<?php if (has_nav_menu('topbar_navigation')) :
              wp_nav_menu(array('theme_location' => 'topbar_navigation', 'menu_class' => 'sf-menu'));
            endif;?>
	</div>
</div>
<!-- Topbar menu -->
	
			
            <?php if(kadence_display_topbar_icons()) : ?>
            <div class="topbar_social">
              <ul>
                <?php global $virtue; $top_icons = $virtue['topbar_icon_menu'];
                foreach ($top_icons as $top_icon) {
                  if(!empty($top_icon['target']) && $top_icon['target'] == 1) {$target = '_blank';} else {$target = '_self';}
                  echo '<li><a href="'.$top_icon['link'].'" target="'.$target.'" title="'.esc_attr($top_icon['title']).'" data-toggle="tooltip" data-placement="bottom" data-original-title="'.esc_attr($top_icon['title']).'">';
                  if($top_icon['url'] != '') echo '<img src="'.$top_icon['url'].'"/>' ; else echo '<i class="'.$top_icon['icon_o'].'"></i>';
                  echo '</a></li>';
                } ?>
              </ul>
            </div>
          <?php endif; ?>
            <?php global $virtue; if(isset($virtue['show_cartcount'])) {
               if($virtue['show_cartcount'] == '1') { 
                if (class_exists('woocommerce')) {
                  global $woocommerce; ?>
                    <ul class="kad-cart-total">
                      <li>
                      <a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php esc_attr_e('View your shopping cart', 'woocommerce'); ?>">
                          <i class="icon-shopping-cart" style="padding-right:5px;"></i> <?php _e('Your Cart', 'virtue');?> <span class="kad-cart-dash">-</span> <?php echo $woocommerce->cart->get_cart_total(); ?>
                      </a>
                    </li>
                  </ul>
                <?php } } }?>
          </div>
        </div><!-- close col-md-6 --> 
        <div class="col-md-6 col-sm-6 kad-topbar-right">
          <div id="topbar-search" class="topbar-widget">
            <?php if(kadence_display_topbar_widget()) { if(is_active_sidebar('topbarright')) { dynamic_sidebar(__('Topbar Widget', 'virtue')); } 
              } else { if(kadence_display_top_search()) {get_search_form();} 
          } ?>
        </div>
        </div> <!-- close col-md-6-->
      </div> <!-- Close Row -->
    </div> <!-- Close Container -->
  </section>
<?php endif; ?>
<?php if(isset($virtue['logo_layout'])) {
  if($virtue['logo_layout'] == 'logocenter') {$logocclass = 'col-md-12'; $menulclass = 'col-md-12';}
  else if($virtue['logo_layout'] == 'logohalf') {$logocclass = 'col-md-6'; $menulclass = 'col-md-6';}
  else {$logocclass = 'col-md-4'; $menulclass = 'col-md-8';} 
} else {$logocclass = 'col-md-4'; $menulclass = 'col-md-8'; }?>
  <div class="container">
    <div class="row">
          <div class="<?php echo esc_attr($logocclass); ?> clearfix kad-header-left">
            <div id="logo" class="logocase">
              <a class="brand logofont" href="<?php echo home_url(); ?>/">
                <?php if (!empty($virtue['x1_virtue_logo_upload']['url'])) { ?>
                  <div id="thelogo">
                    <img src="<?php echo esc_url($virtue['x1_virtue_logo_upload']['url']); ?>" alt="<?php bloginfo('name');?>" class="kad-standard-logo" />
                    <?php if(!empty($virtue['x2_virtue_logo_upload']['url'])) {?>
                    <img src="<?php echo esc_url($virtue['x2_virtue_logo_upload']['url']);?>" class="kad-retina-logo" style="max-height:<?php echo esc_attr($virtue['x1_virtue_logo_upload']['height']);?>px" /> <?php } ?>
                  </div>
                <?php } else { 
                  bloginfo('name'); 
                } ?>
              </a>
              <?php if (isset($virtue['logo_below_text']) && !empty($virtue['logo_below_text'])) { ?>
                <p class="kad_tagline belowlogo-text"><?php echo $virtue['logo_below_text']; ?></p>
              <?php }?>
           </div> <!-- Close #logo -->
       </div><!-- close logo span -->
       <?php if (has_nav_menu('primary_navigation')) : ?>
         <div class="<?php echo esc_attr($menulclass); ?> kad-header-right">
           <nav id="nav-main" class="clearfix" role="navigation">
              <?php wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'sf-menu')); ?>
           </nav> 
          </div> <!-- Close menuclass-->
        <?php endif; ?>       
    </div> <!-- Close Row -->
    <?php if (has_nav_menu('mobile_navigation')) : ?>
           <div id="mobile-nav-trigger" class="nav-trigger">
              <button class="nav-trigger-case mobileclass collapsed" data-toggle="collapse" data-target=".kad-nav-collapse">
                <span class="kad-navbtn"><i class="icon-reorder"></i></span>
                <span class="kad-menu-name"><?php echo __('Menu', 'virtue'); ?></span>
              </button>
            </div>
            <div id="kad-mobile-nav" class="kad-mobile-nav">
              <div class="kad-nav-inner mobileclass">
                <div class="kad-nav-collapse">
                <?php if(isset($virtue['mobile_submenu_collapse']) && $virtue['mobile_submenu_collapse'] == '1') {
                    wp_nav_menu( array('theme_location' => 'mobile_navigation','items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>', 'menu_class' => 'kad-mnav', 'walker' => new kadence_mobile_walker()));
                  } else {
                  wp_nav_menu( array('theme_location' => 'mobile_navigation','items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>', 'menu_class' => 'kad-mnav')); 
                  } ?>
               </div>
            </div>
          </div>   
          <?php  endif; ?> 
  </div> <!-- Close Container -->
  <?php
            if (has_nav_menu('secondary_navigation')) : ?>
  <section id="cat_nav" class="navclass">
    <div class="container">
     <nav id="nav-second" class="clearfix" role="navigation">
     <?php wp_nav_menu(array('theme_location' => 'secondary_navigation', 'menu_class' => 'sf-menu')); ?>
   </nav>
    </div><!--close container-->
    </section>
    <?php endif; ?> 
	<?php if ( is_front_page() ) : ?>
     <?php if (!empty($virtue['virtue_banner_upload']['url'])) { ?> <div class="container"><div class="virtue_banner"><img src="<?php echo esc_url($virtue['virtue_banner_upload']['url']); ?>" /></div></div> <?php } ?>
	<?php endif;?>
</header>