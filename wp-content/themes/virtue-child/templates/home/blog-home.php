<div class="home_blog home-margin clearfix home-padding">
	<?php if(kadence_display_sidebar()) {
		$home_sidebar 	= true;
		$img_width 		= 407;
		$postwidthclass = 'col-md-6 col-sm-6 home-sidebar';
	} else {
		$home_sidebar 	= false;
		$img_width 		= 270;
		$postwidthclass = 'col-md-6 col-sm-6';
	}
	global $virtue; 
	if(isset($virtue['blog_title'])) {
		$btitle = $virtue['blog_title'];
	} else {
		$btitle = __('Latest from the Blog', 'virtue'); 
	} ?>
	<div class="clearfix"><h3 class="hometitle"><?php echo esc_html($btitle); ?></h3></div>
		<div class="row">
		<?php if(isset($virtue['home_post_count'])) {
			$blogcount = $virtue['home_post_count'];
		} else {
			$blogcount = '4';
		} 
		if(!empty($virtue['home_post_type'])) { 
			$blog_cat 	   = get_term_by ('id',$virtue['home_post_type'],'job_listing_category');
			$blog_cat_slug = $blog_cat -> slug;
		} else {
			$blog_cat_slug = '';
		}

			$temp 	  = $wp_query; 
			$wp_query = null; 
			$wp_query = new WP_Query();
			$wp_query->query(array(
				'post_type' 	=> 'job_listing',
				'posts_per_page' => $blogcount,
				'job_listing_category'	 => $blog_cat_slug,
				'meta_query' => array(
					array(
						'key' => '_featured',
						'value' => 1,
						'compare' => '='
					)
				)
				)
			);
			$xyz = 0;
			?>
			<div class="job_listings col-lg-8 col-md-7">
				<div class="list-group list-job-group clearfix">
					<?php
					if ( $wp_query ) : while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>				
						<?php get_job_manager_template_part( 'content', 'home_job_listing' ); ?>					
                    <?php endwhile; else: ?>
						<li class="error-not-found"><?php _e('Sorry, no blog entries found.', 'virtue');?></li>
					<?php endif; ?>
				</div>
			</div> 

			<div class="col-lg-4 col-md-5">
				<div class="slider-box">
					<h2>Bạn muốn thay đổi công việc?</h2>
					<?php echo home_search_job_form ();?>
				</div>
				<div class="spacer spacer-sm "></div>
					<div class="call-to-action centered no no cta__overlay-opacity-25 cta-overlay-color__dark"><div class="cta-inner"><div class="cta-txt"><h2>Cần tuyển nhân tài?</h2><p>Đăng tuyển dụng ngay hôm nay để tin tuyển dụng của bạn tiếp cận đến nhiều ứng viên nhất.</p></div><div class="cta-btn"><a href="dang-tuyen-dung/" class="home-button button">Đăng tuyển dụng</a></div></div></div>
				
			</div>
				
				<?php $wp_query = null; $wp_query = $temp;  // Reset ?>
				<?php wp_reset_query(); ?>

	</div>
</div> <!--home-blog -->