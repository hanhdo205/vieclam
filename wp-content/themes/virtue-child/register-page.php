<section class="slash-bg">
<div id="blog_carousel_container" class="carousel_outerrim">
<div class="blog-carouselcase fredcarousel">
<div id="carouselcontainer">
<div class="caroufredsel_wrapper" style="display: block; text-align: start; float: none; top: auto; right: auto; bottom: auto; left: auto; z-index: auto; width: 100%; margin: 0px;">
<div id="blog_carousel" class="blog_carousel caroufedselclass clearfix">
<div class="tcol-md-6 tcol-sm-6 tcol-xs-6 tcol-ss-12 service-intro">
<div class="margin-medium"></div>
<h5>Tại sao chọn chúng tôi?</h5>
  <li><strong>Miễn phí đăng ký, đăng tin tuyển dụng, nộp hồ sơ ứng tuyển</strong></li>
<em>Chúng tôi lấy tiêu chí phục vụ cộng đồng làm phương châm hoạt động.</em>
  <li><strong>Không thu phí giới thiệu</strong></li>
<em>Nhà tuyển dụng và người tìm việc có thể liên lạc trực tiếp với nhau.</em>
 <li><strong>Tin tuyển dụng được chia sẻ rộng rãi trên các mạng xã hội</strong></li>
<em>Giúp bạn có cơ hội tìm thấy các ứng viên tài năng hơn, tin tuyển dụng của bạn sẽ được chúng tôi chia sẻ trên các trang mạng xã hội như Facebook, Google+, Tweeter, Zing...</em>
  <li><strong>Khách hàng của chúng tôi là những Công ty hàng đầu</strong></li>
<em>Bạn là người tìm việc? Bạn sẽ có cơ hội nộp hồ sơ vào những Công ty hàng đầu với những công việc hấp dẫn.</em>
  <li><strong>Chúng tôi liên tục cập nhật những tính năng mới</strong></li>
<em>Đội ngũ nhân viên nhiệt huyết, trẻ trung và nhiều kinh nghiệm luôn sẵn sàng đem những điều mới mẻ vào dịch vụ của chúng tôi.</em>
  <li><strong>Giúp tiết kiệm thời gian</strong></li>
<em>Đăng tin tuyển dụng - tìm kiếm ứng viên, cập nhật hồ sơ nhanh chóng, dễ dàng tìm được công việc như ý.</em>
</div>
<div class="tcol-md-3 tcol-sm-3 tcol-xs-6 tcol-ss-12 serice-content">
<div class="blog_item grid_item">
<div class="imghoverclass"><img class="iconhover" style="display: block;" src="../wp-content/themes/virtue-child/assets/img/handshake.jpg" alt="nhà tuyển dụng" /></div>
<div class="housekeeping-box"><header>
<h5 class="entry-title">Bạn là nhà tuyển dụng vui lòng đăng ký.</h5>
</header>
<div class="button-action" style="margin-bottom: 20px;"><a class="btn btn-lg btn-primary" href="../dang-ky-nha-tuyen-dung/">NHÀ TUYỂN DỤNG <i class="fa fa-chevron-circle-down"></i></a></div>
<div class="reg-entry-content" style="border-top: 1px solid #e9e9e9;">
<p><strong>Tại sao đăng ký?</strong>
- Đăng công việc để nhận được những hồ sơ phù hợp
- Nhận thông báo hồ sơ qua email</p>
</div>
</div>
</div>
</div>
<div class="tcol-md-3 tcol-sm-3 tcol-xs-6 tcol-ss-12 serice-content">
<div class="blog_item grid_item">
<div class="imghoverclass"><img class="iconhover" style="display: block;" src="../wp-content/themes/virtue-child/assets/img/lookingforajob.jpg" alt="ứng viên" /></div>
<div class="housekeeping-box"><header>
<h5 class="entry-title">Bạn là ứng viên vui lòng đăng ký.</h5>
</header>
<div class="button-action" style="margin-bottom: 20px;"><a class="btn btn-lg btn-primary" href="../dang-ky-nguoi-tim-viec/">ỨNG VIÊN <i class="fa fa-chevron-circle-down"></i></a></div>
<div class="reg-entry-content" style="border-top: 1px solid #e9e9e9;">

<strong>Lợi ích của thành viên</strong>
- Dễ dàng tạo hồ sơ tìm việc
- Ứng tuyển trực tuyến
- Tìm được công việc mơ ước

</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>