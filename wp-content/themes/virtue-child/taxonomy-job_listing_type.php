<?php
/**
 * Job Category
 *
 * @package Virtue by hanhdo
 * @since Virtue by hanhdo
 */

$taxonomy = get_taxonomy( get_queried_object()->taxonomy );

get_header(); ?>

	<div id="pageheader" class="titleclass">
		<div class="container">
		<?php search_job_form ();?>
			<div class="page-header">
			<h1 class="entry-title" itemprop="name">
				Việc làm <?php echo apply_filters('kadence_page_title', kadence_title() ); ?>				
			</h1>
			Tìm thấy <?php echo $wp_query->found_posts;?> việc làm		
		</div><!--container-->
	</div><!--titleclass-->

<div id="content" class="container">
   		<div class="row">
	      	<div class="main <?php echo kadence_main_class(); ?>" role="main">
		      	<?php echo category_description(); ?> 
				<?php if ( have_posts() ) : ?>
				<div class="job_listings">
					<ul class="job_listings">
						<?php while ( have_posts() ) : the_post(); ?>
							<?php get_job_manager_template_part( 'content', 'job_listing' ); ?>
						<?php endwhile; ?>
					</ul>
				</div>
				<?php else : ?>
					<?php get_template_part( 'content', 'none' ); ?>
				<?php endif; ?>

				<?php remove_filter( 'posts_clauses', 'order_featured_job_listing' ); ?>
								<?php
				//Page Navigation
				  if ($wp_query->max_num_pages > 1) :
					virtue_wp_pagenav();
				  endif; ?>

			</div><!-- /.main -->


