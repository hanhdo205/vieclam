<?php
/**
 *
 Template Name: Login Page
 *
 * @package Virtue by hanhdo
 * @since Virtue by hanhdo
 */

get_header(); ?>

	<div id="pageheader" class="titleclass">
		<div class="container">
			<?php get_template_part('templates/page', 'header'); ?>
		</div><!--container-->
	</div><!--titleclass-->

<div id="content" class="container">
   		<div class="row">
	      	<div class="main <?php echo kadence_main_class(); ?>" role="main">
	      		<div class="Login_content col-lg-8"></div>
				
				<div class="cleanlogin-container col-lg-4">
					<?php $login  = (isset($_GET['login']) ) ? $_GET['login'] : 0; 
				if ( $login === "failed" ) {
				    echo '<p class="login-msg"><strong>ERROR:</strong> Invalid username and/or password.</p>';
				} elseif ( $login === "empty" ) {
				    echo '<p class="login-msg"><strong>ERROR:</strong> Username and/or Password is empty.</p>';
				} elseif ( $login === "false" ) {
				    echo '<p class="login-msg"><strong>ERROR:</strong> You are logged out.</p>';
				}
				?>
				<?php
				$args = array(
				    'redirect' => home_url(), 
				    'id_username' => 'user',
				    'id_password' => 'pass',
				   ) 
				;?>
				<?php wp_login_form( $args ); ?>
				<a class="lost-pwd" href="<?php echo site_url(); ?>/lost-password/"><?php _e('Lost Password') ?></a>
				<hr>
				<p class="clear">Bạn chưa có tài khoản đăng nhập?</p>
				<p class="clear">Đăng ký miễn phí <a class="line_bot" href="../dang-ky-tai-khoan/" rel="nofollow"> tại đây.</a></p>
				</div>
					
			</div><!-- /.main -->
