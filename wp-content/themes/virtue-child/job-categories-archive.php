<?php
/**
 *
 Template Name: Job Catrgories Archives
 *
 * @package Virtue by hanhdo
 * @since Virtue by hanhdo
 */

get_header(); ?>
<script src="../wp-content/themes/virtue-child/assets/js/blocksit.js"></script>
<script>
$(document).ready(function() {
	//vendor script
	$('#header')
	.css({ 'top':-50 })
	.delay(1000)
	.animate({'top': 0}, 800);
	
	$('#footer')
	.css({ 'bottom':-15 })
	.delay(1000)
	.animate({'bottom': 0}, 800);
	
	//blocksit define
	$(window).load( function() {
		$('#container').BlocksIt({
			numOfCol: 4,
			offsetX: 8,
			offsetY: 8
		});
	});
	
	//window resize
	var currentWidth = 1200;
	$(window).resize(function() {
		var winWidth = $(window).width();
		var conWidth;
		if(winWidth < 660) {
			conWidth = 440;
			col = 1
		} else if(winWidth < 992) {
			conWidth = 660;
			col = 2
		} else if(winWidth < 1200) {
			conWidth = 992;
			col = 3;
		} else {
			conWidth = 1200;
			col = 4;
		}
		
		if(conWidth != currentWidth) {
			currentWidth = conWidth;
			$('#container').width(conWidth);
			$('#container').BlocksIt({
				numOfCol: col,
				offsetX: 8,
				offsetY: 8
			});
		}
	});
});
</script>
	<div id="pageheader" class="titleclass">
		<div class="container">
		<?php search_job_form ();?>
			<?php get_template_part('templates/page', 'header'); ?>
		</div><!--container-->
	</div><!--titleclass-->

<div id="content" class="container">
   		<div class="row">
	      	<div class="main job-cat-arc <?php echo kadence_main_class(); ?>" role="main">
		      	<?php //hien thi tung category trong trang home
					$cat_args = array(
					  'orderby' => 'id',
					  'order' => 'ASC',
					  'child_of' => 0,
					  'exclude'  => '1'
					);
					$categories =   get_job_categories($cat_args); 
					echo "<div id='container'>";
					foreach($categories as $category) { 
					if ($category->category_parent == 0) {
					$sub = '';
					$child_list = get_job_categories('child_of='.$category->cat_ID.'&hide_empty=0');
					foreach($child_list as $child) {
					$sub .= '<li class="child_list"><a class="subcat" href="' . get_job_category_link( $child->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), __($child->name) ) . '" ' . '> &raquo;&nbsp;' . __($child->name).'</a>('.$child->count.')</li>';
					}
						echo '<div class="grid job_categories_title"><a href="' . get_job_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), __($category->name) ) . '" ' . '>' . __($category->name).'</a><ul>';
						echo $sub;
						echo '</ul></div>';
					}//if ($category
					}//foreach($categories	
					echo "</div>";
					?>
					
			</div><!-- /.main -->

			<div class="page-header">
			<h1 class="entry-title" itemprop="name">
				Tìm kiếm việc làm nhanh theo địa điểm</h1>
		   	</div>
		   	<div class="main job-cat-arc <?php echo kadence_main_class(); ?> blue-slash-bg" role="main">
		   		<ol>
		      	<?php foreach ( get_job_listing_regions() as $cat ) : ?>
		      	<li class="job_region_title"><span class="icon-caret-right"></span>  <a class="subcat" href="<?php echo get_job_region_link($cat->term_id);?>"><?php echo esc_html( $cat->name ); ?></a></li>
		      	<?php endforeach; ?>
				</ol>	
					
			</div><!-- /.main -->
