<style>
				input[type="text"],input[type="tel"],input[type="password"]{
				  width: 100%;
				  height: 34px;
				}
				div#theme-my-login{
					background: url(<?php echo get_stylesheet_directory_uri() ?>/img/bg_login_sucess.png) no-repeat !important;
					background-position: 5px 0px;
					margin-top: 10px;
					min-height: 365px;
					
					min-height: 365px;
				}
				.login p.message {
					padding: 5px;
					border: 1px solid #e6db55;
					background-color: rgba(255,255,224,0.5);
					color: #333;
					}
				</style>
<?php
/*
Template Name: Employer Register Page
*/
?>
<div id="pageheader" class="titleclass">
	<div class="container">
		<?php get_template_part('templates/page', 'header'); ?>
	</div><!--container-->
</div><!--titleclass-->
<?php
if(is_user_logged_in()){
?>
 <div id="content" class="container">
    <div class="row">
      <div class="main <?php echo esc_attr( kadence_main_class() ); ?>" role="main">
    
	<div class="login" id="theme-my-login">
						<p class="message">Bạn đang đăng nhập</p>
					</div>
</div>
<?php
} else {

get_header();
require_once "formvalidator.php";
$show_form=true; 
$successmsg = "";
 ?>
	
  <div id="content" class="container">
    <div class="row single-article" itemscope="" itemtype="http://schema.org/BlogPosting">
      <div class="main <?php echo esc_attr( kadence_main_class() ); ?>" role="main">


<?php
if(isset($_POST['wp-submit'])){

	error_reporting(E_ALL ^ E_DEPRECATED);
	$validator = new FormValidator();
	
	
	$user_id = username_exists( $_POST['user_login'] );

	
		if (!empty($user_id) || email_exists($user_email) == true ) {		
			$validator->addValidation("user_login","num","Tài khoản này đã có người sử dụng, vui lòng kiểm tra lại.");	
		}
	
		$validator->addValidation("user_login","req","Vui lòng nhập tên tài khoản.");
		$validator->addValidation("user_email","req","Vui lòng nhập địa chỉ email.");
		$validator->addValidation("user_email","email","Địa chỉ email không hợp lệ.");
		$validator->addValidation("user_password","req","Vui lòng nhập mật khẩu.");
		$validator->addValidation("user_password2","eqelmnt=user_password","Mật khẩu không khớp.");
		
		$validator->addValidation("companyname","req","Vui lòng nhập tên Công ty.");
		$validator->addValidation("company_info","req","Không được để trống.");
		$validator->addValidation("address","req","Vui lòng nhập địa chỉ Công ty.");
		$validator->addValidation("contactname","req","Vui lòng nhập tên người liên hệ.");
		$validator->addValidation("contactphone","req","Vui lòng nhập số điện thoại liên hệ.");
		$validator->addValidation("contactphone","num","Số điện thoại không hợp lệ.");

		$validator->addValidation("user_dk_dd","req","Vui lòng đồng ý điều khoản của chúng tôi.");
	
	
	
	
	if($validator->ValidateForm())
    {
        
		//cap nhat user meta  
		$user_id = wp_create_user($_POST['user_login'], $_POST['user_password'], $_POST['user_email']);
		

		$pieces = explode(" ", $_POST['companyname']);
		$result = count($pieces);
		$first_name = $pieces[0];
		for ($i=1;$i<$result-1;$i++) {$first_name.= " ".$pieces[$i];}
		$last_name = $pieces[$result-1];
		
		update_user_meta($user_id,'_company_name', $_POST['companyname']);
		if (isset($_POST['website'])) update_user_meta($user_id,'_company_website', $_POST['website']);
		if (isset($_POST['company_size'])) update_user_meta($user_id,'_company_tagline', $_POST['company_size']);
		update_user_meta($user_id,'_company_info', $_POST['company_info']);
		
		// change default display name format
		add_action('user_register', 'registration_save_displayname', 1000);
		function registration_save_displayname($user_id) {
			$pretty_name = $_POST['companyname'];
			wp_update_user( array ('ID' => $user_id, 'display_name'=> $pretty_name) ) ;
		}
		//update_user_meta($user_id,'first_name', $first_name);
		//update_user_meta($user_id,'last_name', $last_name);
		update_user_meta($user_id,'show_admin_bar_front', 'false');

		update_user_meta($user_id,'_job_location', $_POST['address']);
		update_user_meta($user_id,'_contact_name', $_POST['contactname']);
		
		
		update_user_meta($user_id,'_contact_phone', $_POST['contactphone']);
		update_user_meta($user_id,'term_condition', $_POST['user_dk_dd']);	
			
		
		$user_id_role = new WP_User($user_id);
		$user_id_role->set_role('employer');
		
		wp_new_user_notification( $user_id, $_POST['user_password'] );
		
        $show_form=false;
		
    }
    else
    {
		$fail = true;
		//echo '<style> input[type="text"]{border:1px solid red;}</style>';
        $errorvar = "<p class=\"message\">";
 
        $error_hash = $validator->GetErrors();
        foreach($error_hash as $inpname => $inp_err)
        {
			echo '<style> #'.$inpname.' {border:1px solid red;}</style>'; 
          $errorvar .= "<strong>Lỗi</strong>: $inp_err\n<br>";
        }
		$errorvar .= "</p>";
    }

}
if(false == $show_form) {
			?>
				<div class="login" id="theme-my-login">
					<p class="message">Cám ơn bạn đã đăng ký làm thành viên của trang tuyển dụng "Việc làm mỗi ngày". Hãy kiểm tra email để nhận mật khẩu đăng nhập (có thể nằm trong Inbox hoặc SPAM tùy theo bộ lọc email của bạn).<br />Ngay sau khi bạn đăng nhập, bạn có thể bắt đầu tìm cho mình 1 công việc thật ưng ý. Chúc bạn thành công!</p>
				</div>
			<?php }

if(true == $show_form) {
?>
		<div class="col-md-6 register-page">
			<h2>Simply...we find great people for great jobs</h2>
			<div style="padding-bottom:30px;"></div>
			<div data-animation="fadeInLeft" data-animation-delay="0" class="animated fadeInLeft visible">
			<div class="icon-box no normal no circled no icon-box-animated"><div class="icon"><i class="entypo icon-edit"></i></div><div class="icon-box-body"><h4>Xây dựng đội ngũ nhân tài cho doanh nghiệp</h4><em>Thông tin đăng tuyển của bạn sẽ hiển thị trực tuyến trên CareerBuilder.vn và các trang đối tác của chúng tôi trong vòng 30 ngày.</em></div><div class="clearfix"></div></div>
			</div>
			<div data-animation="fadeInLeft" data-animation-delay="200" class="animated fadeInLeft visible">
			<div class="icon-box no normal no circled no icon-box-animated"><div class="icon"><i class="entypo icon-zoom-in"></i></div><div class="icon-box-body"><h4>Không bỏ lỡ nhân tài</h4><em>Truy cập vào hàng trăm ngàn hồ sơ hoàn chỉnh và được cập nhật mới thường xuyên để tìm kiếm những ứng viên phù hợp nhất với vị trí tuyển dụng.</em></div><div class="clearfix"></div></div>
			</div>
			<div data-animation="fadeInLeft" data-animation-delay="200" class="animated fadeInLeft visible">
			<div class="icon-box no normal no circled no icon-box-animated"><div class="icon"><i class="entypo icon-plane"></i></div><div class="icon-box-body"><h4>Tạo sự khác biệt cho thương hiệu công ty</h4><em>Thông tin tuyển dụng của bạn sẽ nổi bật hơn nhờ nội dung đăng tuyển được thiết kế hấp dẫn nhấn mạnh văn hóa và thương hiệu công ty.</em></div><div class="clearfix"></div></div>
			</div>
			<div data-animation="fadeInLeft" data-animation-delay="400" class="animated fadeInLeft visible">
			<div class="icon-box no normal no circled no icon-box-animated"><div class="icon"><i class="entypo icon-key"></i></div><div class="icon-box-body"><h4>Xây dựng thương hiệu tuyển dụng ấn tượng trong mắt ứng viên</h4><em>Quảng cáo tuyển dụng có thể thu hút sự chú ý của các ứng viên tài năng nhờ gắn liên kết trực tiếp đến thông tin tuyển dụng của bạn trên Logo hoặc Banner.</em></div><div class="clearfix"></div></div>
			</div>
			<div class="spacer spacer-xs "></div>
			
		</div>

		<div class="col-md-6 ">
		<?php if($fail) echo $errorvar;?>
		<form name="registerform" id="registerform" action="<?php echo site_url();?>/employer-register/" method="post">
		<fieldset>
		<legend>Thông tin đăng nhập</legend>
			<p>
				<label for="user_login">Tài khoản<span class="description"> *</span></label>
				<input type="text" name="user_login" id="user_login" class="input" value="<?php echo $_POST['user_login'];?>" size="20">
			</p>

			<p>
				<label for="user_email">E-mail<span class="description"> *</span></label>
				<input type="text" name="user_email" id="user_email" class="input" value="<?php echo $_POST['user_email'];?>" size="20">
			</p>
			
			<p>
				<label for="user_password">Mật khẩu<span class="description"> *</span></label>
				<input type="password" name="user_password" id="user_password" class="input" value="<?php echo $_POST['user_password'];?>" size="20">
			</p>
			<p>
				<label for="user_password2">Xác nhận lại mật khẩu<span class="description"> *</span></label>
				<input type="password" name="user_password2" id="user_password2" class="input" value="<?php echo $_POST['user_password2'];?>" size="20">
			</p>
			</fieldset>
			<fieldset>
			<legend>Thông tin Công ty</legend>
			<p>
				<label for="companyname">Tên Công ty<span class="description"> *</span></label>
				<input type="text" name="companyname" id="companyname" class="input" value="<?php echo $_POST['companyname'];?>" size="20">
			</p>
			<p>
				<label for="website">Website<span class="description"> *</span></label>
				<input type="text" name="website" id="website" class="input" value="<?php echo $_POST['website'];?>" size="20">
			</p>
			<p>
				<label for="companysize">Qui mô Công ty</label>
				<div class="select-style isborder">	
					<select class="width_202 valid postform form-control" name="company_size">
						  <option value="">Chọn số nhân viên</option>
						  <option value="1">Ít hơn 10</option>
						  <option value="2">10-20</option>
						  <option value="3">25-99</option>
						  <option value="4">100-499</option>
						  <option value="5">500-999</option>
						  <option value="6">1.000-4.999</option>
						  <option value="7">5.000-9.999</option>
						  <option value="8">10.000-19.999</option>
						  <option value="9">20.000-49.999</option>
						  <option value="10">Hơn 50.000</option>
					</select>
				</div>
			</p>
			<p>
				<label for="company_info">Sơ lược về Công ty<span class="description"> *</span></label>
				<textarea name="company_info" id="company_info" class="company_info" value="<?php echo $_POST['company_info'];?>"></textarea>
				<em>Từ 50 đến 5000 kí tự</em>
			</p>
			<p>
				<label for="address">Địa chỉ Công ty<span class="description"> *</span></label>
				<input type="text" name="address" id="address" class="input" value="<?php echo $_POST['address'];?>" size="20">
			</p>
			<p>
				<label for="contactname">Người liên hệ<span class="description"> *</span></label>
				<input type="text" name="contactname" id="contactname" class="input" value="<?php echo $_POST['contactname'];?>" size="20">
			</p>
			<p>
				<label for="contactphone">Điện thoại liên hệ<span class="description"> *</span></label>
				<input type="text" name="contactphone" id="contactphone" class="input" value="<?php echo $_POST['contactphone'];?>" size="20">
			</p>
			</fieldset>
			<p>
		
				<i><input type="checkbox" name="user_dk_dd" id="user_dk_dd" value="1"  <?php if ( isset($_POST['user_dk_dd']) ) echo 'checked=checked';?>>&nbsp;&nbsp;Tôi đã xem và đồng ý với các <a href="<?php echo site_url();?>/chinh-sach-rieng-tu/" target="_blank">Qui định bảo mật</a> & <a href="<?php echo site_url();?>/thoa-thuan-su-dung/" target="_blank">Thỏa thuận sử dụng</a> của MyCareer.vn.</i>
			</p>

			<p class="submit">
				<input class="button" type="submit" name="wp-submit" id="wp-submit" value="Đăng ký">
			</p>
		</form>
		</div>

<?PHP

}//true == $show_form

?>

</div>

<?php 

}
?>