<?php
/**
 * Job Category
 *
 * @package Virtue by hanhdo
 * @since Virtue by hanhdo
 */

$taxonomy = get_taxonomy( get_queried_object()->taxonomy );

get_header(); ?>

	<div id="pageheader" class="titleclass">
		<div class="container">
			<?php get_template_part('templates/page', 'header'); ?>
		</div><!--container-->
	</div><!--titleclass-->

<div id="content" class="container">
   		<div class="row">
	      	<div class="main <?php echo kadence_main_class(); ?>" role="main">
		      	<?php echo category_description(); ?> 
				<?php if ( have_posts() ) : ?>
				<div class="job_listings">
					<ul class="job_listings">
						<?php while ( have_posts() ) : the_post(); ?>
							<?php get_job_manager_template_part( 'content', 'job_listing' ); ?>
						<?php endwhile; ?>
					</ul>
				</div>
				<?php else : ?>
					<?php get_template_part( 'content', 'none' ); ?>
				<?php endif; ?>

				<?php remove_filter( 'posts_clauses', 'order_featured_job_listing' ); ?>

			</div><!-- /.main -->


