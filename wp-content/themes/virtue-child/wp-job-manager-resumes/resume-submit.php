<?php
/**
 * Resume Submission Form
 */
if ( ! defined( 'ABSPATH' ) ) exit;

wp_enqueue_script( 'wp-resume-manager-resume-submission' );
?>
<form action="<?php echo $action; ?>" method="post" id="submit-resume-form" class="job-manager-form" enctype="multipart/form-data">

	<?php do_action( 'submit_resume_form_start' ); ?>

	<?php if ( apply_filters( 'submit_resume_form_show_signin', true ) ) : ?>

		<?php get_job_manager_template( 'account-signin.php', '', 'wp-job-manager-resumes', RESUME_MANAGER_PLUGIN_DIR . '/templates/' ); ?>

	<?php endif; ?>

	<?php if ( get_option( 'resume_manager_linkedin_import' ) ) : ?>

		<?php get_job_manager_template( 'linkedin-import.php', '', 'wp-job-manager-resumes', RESUME_MANAGER_PLUGIN_DIR . '/templates/' ); ?>

	<?php endif; ?>

	<?php if ( resume_manager_user_can_post_resume() ) : ?>
		<!-- Resume Fields -->
		<?php do_action( 'submit_resume_form_resume_fields_start' ); ?>
		
		<?php foreach ( $resume_fields as $key => $field ) : ?>
		<?php if($key=="candidate_location") : ?>
			<fieldset class="fieldset-<?php esc_attr_e( $key ); ?>">			
				
				<?php
				/*Tag suggest by Hanh*/					
						$city_file = "city_list.ini" ;
							if (file_exists($city_file) && is_readable($city_file))
							{
								$citys=parse_ini_file($city_file,true);
								$html=array();
								foreach ($citys as $city) { 
									array_push($html,"'".$city['iso2']."'");												
								}
							}
							else
							{
								// If the configuration file does not exist or is not readable, DIE php DIE!
								die("Sorry, the $city_file file doesnt seem to exist or is not readable!");
							}

						$array_tags = implode(", ",$html);
						?>
						<script src="<?php echo get_stylesheet_directory_uri();?>/wp-job-manager-resumes/js/jquery-1.9.0.js"></script>
						<script src="<?php echo get_stylesheet_directory_uri();?>/wp-job-manager-resumes/js/jquery-ui-1.10.0.custom.js"></script>
						<script src="<?php echo get_stylesheet_directory_uri();?>/wp-job-manager-resumes/js/jquery.taghandler.js"></script>	
						
						<script type="text/javascript">
							var tag = $.noConflict(true);
						</script>
						<link type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/wp-job-manager-resumes/css/jquery.taghandler.min.css" rel="stylesheet">
						<link type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/wp-job-manager-resumes/css/jquery-ui.css" rel="stylesheet">
						<link type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/wp-job-manager-resumes/css/prettify.css" rel="stylesheet">
						
							<script type="text/javascript">
							tag(function(){
							
									tag(".methodExample").tagHandler({
										assignedTags: [ document.getElementById('candidate_location').value ],
										sortTags:false,
										availableTags: [<?php echo $array_tags; ?>],
										autocomplete: true,
										maxTags:1,
										afterAdd: function() {document.getElementById("candidate_location").value = (tag("#example_get_tags").tagHandler("getTags").join(","));},
										afterDelete: function() {document.getElementById("candidate_location").value = (tag("#example_get_tags").tagHandler("getTags").join(","));}
									});
						
						
						
									prettyPrint();
								})
							</script>
				<label for="<?php esc_attr_e( $key ); ?>"><?php echo $field['label'] . apply_filters( 'submit_resume_form_required_label', $field['required'] ? '' : ' <small>' . __( '(optional)', 'wp-job-manager-resumes' ) . '</small>', $field ); ?></label>
				<div class="field" >
				<ul id="example_get_tags" class="methodExample"></ul>
				</div>		
				<label style="display:none;" for="<?php esc_attr_e( $key ); ?>"><?php echo $field['label'] . apply_filters( 'submit_resume_form_required_label', $field['required'] ? '' : ' <small>' . __( '(optional)', 'wp-job-manager-resumes' ) . '</small>', $field ); ?></label>
				
				<div class="field" style="display:none;">
					<?php $class->get_field_template( $key, $field ); ?>
				</div>
			</fieldset>
		<?php else : ?>
			<fieldset class="fieldset-<?php esc_attr_e( $key ); ?>">
				<label for="<?php esc_attr_e( $key ); ?>"><?php echo $field['label'] . apply_filters( 'submit_resume_form_required_label', $field['required'] ? '' : ' <small>' . __( '(optional)', 'wp-job-manager-resumes' ) . '</small>', $field ); ?></label>
				<div class="field">
					<?php $class->get_field_template( $key, $field ); ?>
				</div>
			</fieldset>
		<?php endif; ?>
		<?php endforeach; ?>

		<?php do_action( 'submit_resume_form_resume_fields_end' ); ?>

		<p>
			<?php wp_nonce_field( 'submit_form_posted' ); ?>
			<input type="hidden" name="resume_manager_form" value="<?php echo $form; ?>" />
			<input type="hidden" name="resume_id" value="<?php echo esc_attr( $resume_id ); ?>" />
			<input type="hidden" name="job_id" value="<?php echo esc_attr( $job_id ); ?>" />
			<input type="hidden" name="step" value="<?php echo esc_attr( $step ); ?>" />
			<input type="submit" name="submit_resume" class="button" value="<?php esc_attr_e( $submit_button_text ); ?>" />
		</p>	
	<?php else : ?>

		<?php do_action( 'submit_resume_form_disabled' ); ?>

	<?php endif; ?>

	<?php do_action( 'submit_resume_form_end' ); ?>
</form>