<style>
				input[type="text"],input[type="tel"],input[type="password"]{
				  width: 100%;
				  height: 34px;
				}
				div#theme-my-login{
					background: url(<?php echo get_stylesheet_directory_uri() ?>/img/bg_login_sucess.png) no-repeat !important;
					background-position: 5px 0px;
					margin-top: 10px;
					min-height: 365px;
					
					min-height: 365px;
				}
				.login p.message {
					padding: 5px;
					border: 1px solid #e6db55;
					background-color: rgba(255,255,224,0.5);
					color: #333;
					}
				</style>
<?php
/*
Template Name: Jobseeker Register Page
*/
?>
<div id="pageheader" class="titleclass">
	<div class="container">
		<?php get_template_part('templates/page', 'header'); ?>
	</div><!--container-->
</div><!--titleclass-->
<?php
if(is_user_logged_in()){
?>
 <div id="content" class="container">
    <div class="row">
      <div class="main <?php echo esc_attr( kadence_main_class() ); ?>" role="main">
    
	<div class="login" id="theme-my-login">
						<p class="message">Bạn đang đăng nhập</p>
					</div>
</div>
<?php
} else {

get_header();
require_once "formvalidator.php";
$show_form=true; 
$successmsg = "";
 ?>
	
  <div id="content" class="container">
    <div class="row">
      <div class="main <?php echo esc_attr( kadence_main_class() ); ?>" role="main">


<?php
if(isset($_POST['wp-submit'])){

	error_reporting(E_ALL ^ E_DEPRECATED);
	$validator = new FormValidator();
	
	
	$user_id = username_exists( $_POST['user_login'] );

	
		if (!empty($user_id) || email_exists($user_email) == true ) {		
			$validator->addValidation("user_login","num","Tài khoản này đã có người sử dụng, vui lòng kiểm tra lại.");	
		}
	
		$validator->addValidation("user_login","req","Vui lòng nhập tên tài khoản.");
		$validator->addValidation("user_email","req","Vui lòng nhập địa chỉ email.");
		$validator->addValidation("user_email","email","Địa chỉ email không hợp lệ.");
		$validator->addValidation("user_password","req","Vui lòng nhập mật khẩu.");
		$validator->addValidation("user_password2","eqelmnt=user_password","Mật khẩu gõ lại không khớp.");
		
		$validator->addValidation("user_ht","req","Vui lòng nhập họ và tên.");
		
		$validator->addValidation("user_dk_dd","req","Vui lòng đồng ý điều khoản của chúng tôi.");
	
	
	
	
	if($validator->ValidateForm())
    {
        
		//cap nhat user meta  
		$user_id = wp_create_user($_POST['user_login'], $_POST['user_password'], $_POST['user_email']);

		$pieces = explode(" ", $_POST['user_ht']);
		$result = count($pieces);
		$first_name = $pieces[0];
		for ($i=1;$i<$result-1;$i++) {$first_name.= " ".$pieces[$i];}
		$last_name = $pieces[$result-1];
		
		update_user_meta($user_id,'fullname', $_POST['user_ht']);
		update_user_meta($user_id,'first_name', $first_name);
		update_user_meta($user_id,'last_name', $last_name);
		update_user_meta($user_id,'show_admin_bar_front', 'false');
		
		//update_user_meta($user_id,'user_level_dd', $_POST['user_level_dd']);
		update_user_meta($user_id,'term_condition', $_POST['user_dk_dd']);
		
		$user_id_role = new WP_User($user_id);
		$user_id_role->set_role('candidate');
		
		wp_new_user_notification( $user_id, $_POST['user_password'] );
		
		//wp_set_auth_cookie( $user_id, false, is_ssl() );
		//wp_redirect( admin_url( 'profile.php' ) );
				
        $show_form=false;
		
    }
    else
    {
		$fail = true;
		//echo '<style> input[type="text"]{border:1px solid red;}</style>';
        $errorvar = "<p class=\"message\">";
 
        $error_hash = $validator->GetErrors();
        foreach($error_hash as $inpname => $inp_err)
        {
			echo '<style> #'.$inpname.' {border:1px solid red;}</style>'; 
          $errorvar .= "<strong>Lỗi</strong>: $inp_err\n<br>";
        }
		$errorvar .= "</p>";
    }

}
if(false == $show_form) {
			?>
    
					<div class="login" id="theme-my-login">
						<p class="message">Cám ơn bạn đã đăng ký làm thành viên của trang tuyển dụng "Việc làm mỗi ngày". Hãy kiểm tra email để nhận mật khẩu đăng nhập (có thể nằm trong Inbox hoặc SPAM tùy theo bộ lọc email của bạn).<br />Ngay sau khi bạn đăng nhập, bạn có thể bắt đầu tìm cho mình 1 công việc thật ưng ý. Chúc bạn thành công!</p>
					</div>
			<?php }

if(true == $show_form) {
?>
   
		<div class="col-md-6 register-page">
			<h2>Lợi ích của thành viên</h2>
			<div style="padding-bottom:30px;"></div>
			<div data-animation="fadeInLeft" data-animation-delay="0" class="animated fadeInLeft visible">
			<div class="icon-box no normal no circled no icon-box-animated"><div class="icon"><i class="entypo icon-pencil"></i></div><div class="icon-box-body"><h4>Dễ dàng tạo hồ sơ</h4><em>Chỉ cần đăng ký tài khoản, bạn có thể tạo cho mình nhiều hồ sơ để ứng tuyển công việc mọi lúc, mọi nơi. </em></div><div class="clearfix"></div></div>
			</div>
			<div data-animation="fadeInLeft" data-animation-delay="200" class="animated fadeInLeft visible">
			<div class="icon-box no normal no circled no icon-box-animated"><div class="icon"><i class="entypo icon-thumbs-up-alt"></i></div><div class="icon-box-body"><h4>Ứng tuyển trực tuyến</h4><em>Chọn công việc phù hợp và ứng tuyển trực tuyến, nhà tuyển dụng sẽ nhận hồ sơ của bạn ngay lập tức.</em></div><div class="clearfix"></div></div>
			</div>
			<div data-animation="fadeInLeft" data-animation-delay="400" class="animated fadeInLeft visible">
			<div class="icon-box no normal no circled no icon-box-animated"><div class="icon"><i class="entypo icon-star"></i></div><div class="icon-box-body"><h4>Công việc mơ ước</h4><em>Các công việc đăng tuyển được chọn lọc từ hàng ngàn công việc hàng đầu, giúp bạn có được việc làm mơ ước.</em></div><div class="clearfix"></div></div>
			</div>
			<div class="spacer spacer-xs "></div>
			
		</div>

		<div class="col-md-6 ">
		<?php if($fail) echo $errorvar;?>
		<form name="registerform" id="registerform" action="<?php echo site_url();?>/jobseeker-register/" method="post">
		<fieldset>
		<legend>Thông tin đăng nhập</legend>	
			<p>
				<label for="user_login">Tên đăng nhập<span class="description"> *</span></label>
				<input type="text" name="user_login" id="user_login" class="input" value="<?php echo $_POST['user_login'];?>" size="20">
			</p>

			<p>
				<label for="fullname">Họ và tên <span class="description"> *</span></label>
				<input type="text" name="user_ht" id="user_ht" class="input" value="<?php echo $_POST['user_ht'];?>" size="20">
			</p>

			<p>
				<label for="user_email">E-mail<span class="description"> *</span></label>
				<input type="text" name="user_email" id="user_email" class="input" value="<?php echo $_POST['user_email'];?>" size="20">
			</p>
			
			<p>
				<label for="user_password">Mật khẩu<span class="description"> *</span></label>
				<input type="password" name="user_password" id="user_password" class="input" value="<?php echo $_POST['user_password'];?>" size="20">
			</p>

			<p>
				<label for="user_password2">Xác nhận lại mật khẩu<span class="description"> *</span></label>
				<input type="password" name="user_password2" id="user_password2" class="input" value="<?php echo $_POST['user_password2'];?>" size="20">
			</p>
			</fieldset>
			<p>		
				<i><input type="checkbox" name="user_dk_dd" id="user_dk_dd" value="1"  <?php if ( isset($_POST['user_dk_dd']) ) echo 'checked=checked';?>>&nbsp;&nbsp;Tôi đã xem và đồng ý với các <a href="<?php echo site_url();?>/chinh-sach-rieng-tu/" target="_blank">Qui định bảo mật</a> & <a href="<?php echo site_url();?>/thoa-thuan-su-dung/" target="_blank">Thỏa thuận sử dụng</a> của MyCareer.vn.</i>
			</p>
			
			<p class="submit">
				<input class="button" type="submit" name="wp-submit" id="wp-submit" value="Đăng ký">
			</p>
		</form>
		</div>

<?PHP

}//true == $show_form

?>

</div>

<?php 

}
?>