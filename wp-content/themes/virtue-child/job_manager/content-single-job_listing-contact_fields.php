<?php
/**
 * Single view Job meta box
 *
 * Hooked into single_job_listing_start priority 20
 *
 * @since  1.14.0
 */
global $post;
?>
<ul>
<li><?php _e( 'Expire Date: ', 'wp-job-manager' ); ?><?php the_deadline(); ?></li>
<li><?php _e( 'Contact person: ', 'wp-job-manager' ); ?><?php the_contact_name(); ?></li>
<li><?php _e( 'Preferred Language: ', 'wp-job-manager' ); ?><?php the_language(); ?></li>
</ul>

