<?php if ( $apply = get_the_job_application_method() ) :
	//wp_enqueue_script( 'wp-job-manager-job-application' );
	?><?php global $post; ?>
	<div class="job_application application">
		<?php do_action( 'job_application_start', $apply ); ?>
		
		<!--<input type="button" class="application_button button" value="<?php _e( 'Apply for job', 'wp-job-manager' ); ?>" />-->
		
		<div class="cta-btn">
		
		<a href="#modal" class="application_button button">Nộp hồ sơ</a>
		</div>
		
		<div id="modal">
			<div class="modal-content">
				<div class="header">
					<a href="#" class="btn mfp-close">x</a>
					<h2>Bạn đang ứng tuyển vị trí <?php echo esc_attr( $post->post_title ); ?></h2>
				</div>
				<div class="copy">
					<div class="application_details">
						<?php
							/**
							 * job_manager_application_details_email or job_manager_application_details_url hook
							 */
							do_action( 'job_manager_application_details_' . $apply->type, $apply );
						?>
					</div>
				</div>
				<div class="cf footer"></div>
			</div>
		<div class="overlay"></div>
		</div>
		<?php do_action( 'job_application_end', $apply ); ?>
	</div>
<?php endif; ?>


