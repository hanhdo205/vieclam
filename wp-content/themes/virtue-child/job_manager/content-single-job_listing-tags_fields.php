<?php
/**
 * Single view Job meta box
 *
 * Hooked into single_job_listing_start priority 20
 *
 * @since  1.14.0
 */
global $post;
?>
<div class="level"><?php display_categories_data();?></div>
<div class="level"><?php _e( 'Job Level: ', 'wp-job-manager' ); ?><?php the_job_level(); ?></div>
<?php display_job_tags_data(); ?>


