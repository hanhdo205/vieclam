<?php
/**
 * Job Submission Form
 */
if ( ! defined( 'ABSPATH' ) ) exit;


global $job_manager;
?>

<form action="<?php echo esc_url( $action ); ?>" method="post" id="submit-job-form" class="job-manager-form" enctype="multipart/form-data">

	<?php if ( apply_filters( 'submit_job_form_show_signin', true ) ) : ?>

		<?php get_job_manager_template( 'account-signin.php' ); ?>

	<?php endif; ?>

	<?php if ( job_manager_user_can_post_job() && (current_user_can( 'employer' )||current_user_can( 'edit_posts' )) ) : ?>
		<!-- Job Information Fields -->
		<?php do_action( 'submit_job_form_job_fields_start' ); ?>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
		<?php foreach ( $job_fields as $key => $field ) : ?>
		<?php if($key=="job_currency") : ?>
		<div class="currency-field">
			<?php get_job_manager_template( 'form-fields/' . $field['type'] . '-field.php', array( 'key' => $key, 'field' => $field ) ); ?>
		</div>
		<?php else : ?>
			<fieldset class="fieldset-<?php esc_attr_e( $key ); ?>">
				<label for="<?php esc_attr_e( $key ); ?>"><?php echo $field['label'] . apply_filters( 'submit_job_form_required_label', $field['required'] ? '' : ' <small>' . __( '(optional)', 'wp-job-manager' ) . '</small>', $field ); ?></label>
				<div class="field <?php echo $field['required'] ? 'required-field' : ''; ?>">
					<?php get_job_manager_template( 'form-fields/' . $field['type'] . '-field.php', array( 'key' => $key, 'field' => $field ) ); ?>
				</div>
			</fieldset>
		<?php endif; ?>
		<?php endforeach; ?>
		<script>
			  	$(function() {				
					$( "#job_deadline" ).datepicker({ dateFormat: "dd/mm/yy" });
			  	});
		</script> 
		<?php do_action( 'submit_job_form_job_fields_end' ); ?>

		<!-- Company Information Fields -->
		<?php if ( $company_fields ) : ?>
			<?php wp_enqueue_script( 'wp-job-manager-job-application' );?>
			<div class="job_application">
				<input type="button" class="application_button" value="+ <?php _e( 'Company Details', 'wp-job-manager' ); ?>" />
				
				<div class="application_details">
					<h2><?php _e( 'Company Details', 'wp-job-manager' ); ?></h2>

					<?php do_action( 'submit_job_form_company_fields_start' ); ?>
	
					<?php foreach ( $company_fields as $key => $field ) : ?>
						<fieldset class="fieldset-<?php esc_attr_e( $key ); ?>">
							<label for="<?php esc_attr_e( $key ); ?>"><?php echo $field['label'] . apply_filters( 'submit_job_form_required_label', $field['required'] ? '' : ' <small>' . __( '(optional)', 'wp-job-manager' ) . '</small>', $field ); ?></label>
							<div class="field <?php echo $field['required'] ? 'required-field' : ''; ?>">
								<?php get_job_manager_template( 'form-fields/' . $field['type'] . '-field.php', array( 'key' => $key, 'field' => $field ) ); ?>
							</div>
						</fieldset>
					<?php endforeach; ?>
					
					<?php do_action( 'submit_job_form_company_fields_end' ); ?>
				<?php endif; ?>
				</div>
			</div>
		<p>
			<input type="hidden" name="job_manager_form" value="<?php echo $form; ?>" />
			<input type="hidden" name="job_id" value="<?php echo esc_attr( $job_id ); ?>" />
			<input type="hidden" name="step" value="<?php echo esc_attr( $step ); ?>" />
			<input type="submit" name="submit_job" class="button" value="<?php esc_attr_e( $submit_button_text ); ?>" />
		</p>
	<?php else : ?>

		<?php do_action( 'submit_job_form_disabled' ); ?>

	<?php endif; ?>
</form>