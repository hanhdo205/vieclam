<?php
/**
 * Single view Company information box
 *
 * Hooked into single_job_listing_start priority 30
 *
 * @since  1.14.0
 */

if ( ! get_the_company_name() ) {
	return;
}
?>
<fieldset>
	<legend>Giới thiệu về Công ty</legend>
<div class="company" itemscope itemtype="http://data-vocabulary.org/Organization">
	<?php the_company_logo(); ?>

	<p class="name">
		<?php if ( $website = get_the_company_website() ) : ?>
			<a class="website" href="<?php echo esc_url( $website ); ?>" itemprop="url" target="_blank" rel="nofollow"><?php _e( 'Website', 'wp-job-manager' ); ?></a>
		<?php endif; ?>
		<?php //the_company_twitter(); ?>
		<?php the_company_name( '<strong itemprop="name">', '</strong>' ); ?>
		<p class="location" itemprop="jobLocation" style="margin:0 0 0 3em;padding:0 0 0 1em; line-height:1.5em;"><?php the_job_address(); ?></p>
		<p class="contact" itemprop="jobLocation" style="margin:0 0 0 3em;padding:0 0 0 1em; line-height:1.5em;"><?php _e( 'Contact person: ', 'wp-job-manager' ); ?><?php the_contact_name(); ?></p>
	<p class="company-size" itemprop="jobLocation" style="margin:0 0 0 3em;padding:0 0 0 1em; line-height:1.5em;">
	<?php //the_company_tagline( '<p class="tagline">', '</p>' ); ?>
	<?php $level = the_company_tagline( '', '',0 );if ($level) {
		$size_arr = array(         
			  1=>'Ít hơn 10',
			  2=>'10-20',
			  3=>'25-99',
			  4=>'100-499',
			  5=>'500-999',
			  6=>'1.000-4.999',
			  7=>'5.000-9.999',
			  8=>'10.000-19.999',
			  9=>'20.000-49.999',
			  10=>'Hơn 50.000'
        );
		if ($size_arr[$level])
		echo "Qui mô Công ty: ".$size_arr[$level];
	}; ?></p>
	<div style="margin-top:10px;"><?php the_company_info( '<div class="comment more">', '</div>' );?></div>
	</p>
	<?php //the_company_video(); ?>
</div>
</fieldset>
<script>
$(document).ready(function() {
    var showChar = 150;
    var ellipsestext = "...";
    var moretext = "xem thêm <i class='icon-angle-down'></i>";
    var lesstext = "thu gọn <i class='icon-angle-up'></i>";
    $('.more').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });
 
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
});
</script>