��    X      �     �      �     �  :   �  
   �  #   �     
  	   *  S   4  6   �     �  !   �  
   �     �     		     	     "	     9	     R	     d	     z	     �	  
   �	  2   �	  #   �	     �	  
   

     
  z   /
     �
     �
     �
     �
     �
     �
  &        8     ?     K     P  &   o     �  	   �     �     �  	   �     �     �     �  6        D  Z   K     �     �     �     �  	   �     �     �     �       1        >     P     Y     `     m     �     �     �     �     �     �     �     �     �  '   �          $     )     .  3   E     y  
   �     �  	   �     �  
   �     �    �     �  L   �     )  *   =  /   h     �  U   �  G   �     D  *   Z     �     �     �     �     �     �               1     D  
   L  <   W  #   �  /   �     �     �  �        �     �            !   &  )   H     r     �     �     �  &   �  ,   �               %     C     O     \     {  $   �  A   �     �  _        a     o  &   �     �     �     �     �  
   �  
     M        Z     q     ~     �     �  	   �     �     �  "   �     �          %     1     8  .   F     u     �     �     �  B   �     �       -        D     R     [  $   h     6   4       5                 *              &            #       P       ?       X   -      !       K                  <          B   Q   N       %           "                 V   S   9   E   A      )   (                      8   2   C       G      $           0   I   :   D              	   R      .   ;          H       3   ,          7       1           M       W   =   '                       L       U   >   O   
              +               @      J       /   F   T    %s ago %s draft updated. <a target="_blank" href="%s">Preview</a> %s expired %s published. <a href="%s">View</a> %s restored to revision from %s %s saved. %s scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview</a> %s submitted. <a target="_blank" href="%s">Preview</a> %s updated. %s updated. <a href="%s">View</a> (optional) Account Creation Account Required Add-ons Allow account creation Application Email or URL Application email Application email/URL Applied Approve Approve %s Automatically Generate Username from Email Address Brief description about the company Briefly describe your company Categories Choose a category&hellip; Choose whether to enable categories. Categories must be setup by an admin to allow users to choose them during submission. Company Details Company Name Company name Contact person:  Custom field deleted. Custom field updated. Default page title (wizard)Post a Job Delete Description Edit Enable categories for listings Enable category multiselect by default Enter the name of the company Expire %s Expire Date:  Expires Featured? Filled Positions Filled? Hide filled positions How many listings should be shown per page by default? ID: %d If enabled, the category select box will default to a multiselect on the [jobs] shortcode. Job Level:  Job Listings Job Location Job Submission Job Title Job category Job type Keyword Keywords Leave this blank if the location is not important Listings Per Page Location M j, Y M j, Y @ G:i Multi-select Categories Position Posted Posted %s ago Preferred Language:  Professional Title Select category Settings Sign out Status Submitting listings requires an account Tagline Type View WP Job Manager Add-ons You are currently signed in as <strong>%s</strong>. Your account Your email Your full name Your name by %s by a guest e.g. "London" Project-Id-Version: WP Job Manager
Report-Msgid-Bugs-To: http://wordpress.org/tag/WP-Job-Manager
POT-Creation-Date: 2015-07-28 10:31+0700
PO-Revision-Date: 2015-07-28 13:14+0700
Last-Translator: hanhdo <hanhdo205@gmail.com>
Language-Team: Vietnamese (http://www.transifex.com/projects/p/wp-job-manager/language/vi/) <hanhdo205@gmail.com>
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 1.5.5
X-Poedit-Basepath: ../
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: ..
X-Poedit-SearchPath-2: ..
 %s trước %s bản nháp đã cập nhật. <a target="_blank" href="%s">Xem thử</a> %s đã hết hạn %s đã xuất bản. <a href="%s">Xem</a> %s đã khôi phục lại từ phiên bản %s %s đã lưu. %s scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Xem thử</a> %s đã được đưa lên. <a target="_blank" href="%s">Xem thử</a> %s đã cập nhật. %s đã cập nhật. <a href="%s">Xem</a> (không bắt buộc) Tạo tài khoản Yêu cầu tài khoản Tiện ích Cho phép tạo tài khoản Email liên hệ hoặc URL Email liên hệ Email liên hệ/URL Đã ứng tuyển Duyệt Duyệt %s Tự động tạo tên đăng nhập từ thư điện tử Mô tả ngắn gọn về Công ty Mô tả ngắn gọn về Công ty của bạn Chuyên mục Chọn ngành nghề &hellip; Choose whether to enable categories. Hạng mục cần được cấu hình với quản trị viên để có thể cho phép người dùng chọn mục đó trong quá trình đăng tải thông tin. Thông tin Công ty Tên Công ty Tên Công ty Người liên hệ:  Trường tùy chỉnh đã xóa. Trường tùy chỉnh đã cập nhật. Đăng tuyển dụng Xóa Nội dung tuyển dụng Sửa Kích hoạt các mục cho danh sách Mặc định cho phép chọn nhiều mục Nhập tên Công ty Hết hạn %s Thời hạn nộp hồ sơ:  Hết hạn Nổi bật? Vị trí đã đăng ký hêt Đã đăng ký đủ? Ẩn vị trí đã đăng ký đủ Mặc định có bao nhiêu danh sách hiển thị mối trang? Mã: %d Nếu cho phép, the category select box will default to a multiselect on the [jobs] shortcode. Cấp bậc:  Danh sách việc Nơi làm việc (tỉnh/thành phố) Đăng việc Tiêu đề công việc Ngành nghề Hình thức làm việc Từ khóa Từ khóa Bạn có thể để trống ô này nếu cảm thấy không cần thiết Danh sách mỗi trang Địa chỉ M j, Y M j, Y @ G:i Chọn nhiều mục Vị trí Đã đăng Đăng tuyển %s trước Ngôn ngữ trình bày hồ sơ:  Tiêu đề hồ sơ Chọn chuyên mục Cấu hình Thoát Trạng thái Danh sách đăng lên yêu cầu tài khoản Giới thiệu Công ty Kiểu Xem WP Job Manager Tiện Ích Bạn đang đăng nhập bằng tài khoản <strong>%s</strong>. Tài khoản Địa chỉ email Nhập đầy đủ họ và tên của bạn Họ và tên bởi %s bởi khách 21 Nguyễn Trãi, Quận 1, Tp. HCM 