<?php global $post; ?>
<div class="list-group-item">
	<div class="list-group-item-heading">
		<a href="<?php the_job_permalink(); ?>">
			<?php the_title(); ?>
		</a>
	</div>
	<div class="list-group-item-text">
		<p class="priority-data">
			<?php the_company_name( '', '' ); ?>
		</p>
	</div>				
</div>