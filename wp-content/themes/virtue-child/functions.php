<?php
	wp_enqueue_style('virtue_child_skin', get_stylesheet_directory_uri() . '/assets/css/skins/citrus.css', false, null);
	wp_register_script('menubar', get_stylesheet_directory_uri() . '/assets/js/top-bar.js', false, '20111130', true);
	wp_enqueue_script('menubar');
 
function my_child_theme_setup() {
    load_child_theme_textdomain( 'virtue', get_stylesheet_directory_uri() . '/languages' );
}
 add_action( 'after_setup_theme', 'my_child_theme_setup' );
//Jetpack

/*
How to run Jetpack from localhost?
Add this to your wp-config.php:

const JETPACK_DEV_DEBUG = TRUE;
*/

function jptweak_remove_share() {
    remove_filter( 'the_content', 'sharing_display',19 );
    remove_filter( 'the_excerpt', 'sharing_display',19 );
    if ( class_exists( 'Jetpack_Likes' ) ) {
        remove_filter( 'the_content', array( Jetpack_Likes::init(), 'post_likes' ), 30, 1 );
    }
}
 
add_action( 'loop_start', 'jptweak_remove_share' );

//Jetpack

//Bookmark
global $job_manager_bookmarks;
remove_action( 'single_job_listing_meta_after', array( $job_manager_bookmarks, 'bookmark_form' ) );
remove_action( 'single_resume_start', array( $job_manager_bookmarks, 'bookmark_form' ) );

add_action( 'bookmark_at_sidebar', array( $job_manager_bookmarks, 'bookmark_form' ) );
//Bookmark

function add_form_title() {
    echo '<h2 class="modal-title">' . __( 'Apply', 'virtue' ) . '</h2>';
  }  

//add_action( 'job_application_form_fields_start', 'add_form_title' );


// Add your own function to filter the fields
add_filter( 'submit_job_form_fields', 'custom_submit_job_form_fields' );

// This is your function which takes the fields, modifies them, and returns them
// You can see the fields which can be changed here: https://github.com/mikejolley/WP-Job-Manager/blob/master/includes/forms/class-wp-job-manager-form-submit-job.php
function custom_submit_job_form_fields( $fields ) {
    // Here we target one of the job fields (job_title) and change it's label
    $fields['job']['job_description']['description'] = "Mô tả công việc / Yêu cầu / Quyền lợi - chế độ...";
	$fields['company']['company_logo']['description'] = "Hỗ trợ định dạng .jpg, .png, .gif, nhỏ hơn 512KB";
    // And return the modified fields
    return $fields;
}

// Add your own function to filter the fields
add_filter( 'submit_resume_form', 'custom_submit_resumes_form_fields' );

// This is your function which takes the fields, modifies them, and returns them
// You can see the fields which can be changed here: https://github.com/mikejolley/WP-Job-Manager/blob/master/includes/forms/class-wp-job-manager-form-submit-job.php
function custom_submit_resumes_form_fields( $fields ) {
    // Here we target one of the job fields (job_title) and change it's label
    $fields['resume_fields']['candidate_photo']['description'] = "Hỗ trợ định dạng .jpg, .png, .gif, nhỏ hơn 512KB";
    // And return the modified fields
    return $fields;
}


//Send an email to the employer when a job listing is approved
if ( ! function_exists( 'listing_published_send_email' ) ) :
function listing_published_send_email( $post = null ) {
   $job_listing = get_post($post);

   $author = get_userdata($job_listing->post_author);

   $message = "Xin chào Quý (Công ty) ".$author->_company_name.",
      Tin tuyển dụng với tiêu đề ".$job_listing->post_title." của Quý Công ty đã được đăng tại link sau ".get_permalink( $post ).".
      Chúc Quý Công ty tuyển dụng được ứng viên phù hợp!
   ";
   wp_mail($author->user_email, "Tin tuyển dụng đã được đăng", $message);
}
endif;
add_action('publish_job_listing', 'listing_published_send_email');

/**
 * Display or retrieve the current company name with optional content.
 *
 * @access public
 * @param mixed $id (default: null)
 * @return void
 */
function the_company_info( $before = '', $after = '', $echo = true, $post = null ) {
	$company_info = get_the_company_info( $post );

	if ( strlen( $company_info ) == 0 )
		return;

	$company_info = nl2br( $company_info ) ;
	$company_info = $before . $company_info . $after;

	if ( $echo )
		echo $company_info;
	else
		return $company_info;
}
function get_the_company_info( $post = null ) {
	$post = get_post( $post );
	if ( $post->post_type !== 'job_listing' ) {
		return '';
	}

	return apply_filters( 'the_company_info', $post->_company_info, $post );
}

/**
 * Displays job meta data on the single job page
 */
function job_listing_tags_fields_display() {
	get_job_manager_template( 'content-single-job_listing-tags_fields.php', array() );
}
//add_action( 'single_job_listing_meta_after', 'job_listing_tags_fields_display' );

/**
 * Displays job meta data on the single job page
 */
function job_listing_contact_fields_display() {
	get_job_manager_template( 'content-single-job_listing-contact_fields.php', array() );
}
//add_action( 'single_job_listing_info_end', 'job_listing_contact_fields_display', 20 );

//the_contact_name function.
function the_contact_name( $post = null ) {
	$name = get_the_contact_name( $post );
	if ( $name ) {
		echo $name;
	} else {
		echo apply_filters( 'the_job_contact_name_text', __( 'N/A', 'virtue' ) );
	}
}

//get_the_job_contact_name function.
function get_the_contact_name( $post = null ) {
	$post = get_post( $post );
	if ( $post->post_type !== 'job_listing' )
		return;
	return apply_filters( 'the_contact_name', $post->_job_contact_name, $post );
}

//the_deadline function.
function the_deadline( $post = null ) {
	$deadline = get_the_job_deadline( $post );
	echo $deadline;
}

//get_the_job_deadline function.
function get_the_job_deadline( $post = null ) {
	$post = get_post( $post );
	if ( $post->post_type !== 'job_listing' )
		return;
	return apply_filters( 'the_deadline', $post->_job_deadline, $post );
}

//the_jobtimes function.
function the_jobtimes( $post = null ) {
	$jobtimes = get_the_job_times( $post );
	if ($jobtimes) {
      $lev_arr = array(
          1=>'Toàn thời gian',
          2=>'Bán thời gian',
          3=>'Freelance',
          4=>'Thời vụ',
          5=>'Thực tập'          
        );

    }
	echo $lev_arr[$jobtimes];
}

//get_the_job_deadline function.
function get_the_job_times( $post = null ) {
	$post = get_post( $post );
	if ( $post->post_type !== 'job_listing' )
		return;
	return apply_filters( 'the_jobtimes', $post->_job_times, $post );
}

//the_language function.
function the_language( $post = null ) {
	$lang = get_the_language( $post );
	$lan_arr = array(1=>'Tiếng Việt',2=>'Tiếng Anh',3=>'Bất kỳ');
	echo $lan_arr[$lang];
}

//get_the_language function.
function get_the_language( $post = null ) {
	$post = get_post( $post );
	if ( $post->post_type !== 'job_listing' )
		return;
	return apply_filters( 'the_language', $post->_job_language, $post );
}

//the_job_level function.
function the_job_level( $post = null ) {
	$level = get_the_job_level( $post );
	$lev_arr = array(1=>'Mới tốt nghiệp/Thực tập sinh',2=>'Nhân viên',3=>'Trưởng nhóm/Giám sát',4=>'Trưởng phòng',5=>'Phó giám đốc',6=>'Giám đốc',7=>'Tổng giám đốc điều hành',8=>'Phó chủ tịch',9=>'Chủ tịch');
	echo $lev_arr[$level];
}

//get_the_level function.
function get_the_job_level( $post = null ) {
	$post = get_post( $post );
	if ( $post->post_type !== 'job_listing' )
		return;
	return apply_filters( 'the_job_level', $post->_job_level, $post );
}

//Changing the job slug/permalink
add_filter( 'submit_job_form_save_job_data', 'custom_submit_job_form_save_job_data', 10, 5 );

function custom_submit_job_form_save_job_data( $data, $post_title, $post_content, $status, $values ) {

    $job_slug   = array();

	// Prepend with company name
	/*if ( ! empty( $values['company']['company_name'] ) )
		$job_slug[] = $values['company']['company_name'];*/

	// Prepend location
	/*if ( ! empty( $values['job']['job_location'] ) )
		$job_slug[] = $values['job']['job_location'];*/

	$job_slug[] = $post_title; 
    
    $data['post_name'] = sanitize_title( implode( '-', $job_slug ) );
    
    return $data;
}

//Adding the Job ID to the base URL
function job_listing_post_type_link( $permalink, $post ) {
    // Abort if post is not a job
    if ( $post->post_type !== 'job_listing' )
    	return $permalink;

    // Abort early if the placeholder rewrite tag isn't in the generated URL
    if ( false === strpos( $permalink, '%' ) )
    	return $permalink;

    $find = array(
    	'%post_id%'
    );

    $replace = array(
    	$post->ID
    );

    $replace = array_map( 'sanitize_title', $replace );

    $permalink = str_replace( $find, $replace, $permalink );

    return $permalink;
}
add_filter( 'post_type_link', 'job_listing_post_type_link', 10, 2 );

function change_job_listing_slug( $args ) {
  $args['rewrite']['slug'] = 'viec-lam/%post_id%';
  return $args;
}
add_filter( 'register_post_type_job_listing', 'change_job_listing_slug' );

/**
 * Add rewrite tags for the data we want to insert and define the structure of the permalink.
 *
 * You need to go to Settings > Permalinks and save for these rules to be added.
 */
function add_resume_rewrite_rules(){
  add_rewrite_tag( '%resume%', '([^/]+)', 'resume=' );
  add_rewrite_tag( '%candidate_title%', '([^/]+)', 'candidate_title=' );
  add_permastruct( 'resume', 'resumes/%candidate_title%/%resume%', true );
}
add_action( 'init', 'add_resume_rewrite_rules', 11 );
/**
 * Add data to the permalinks
 * @param  string $permalink
 * @param  WP_Post $post
 * @param  bool
 * @return string
 */
function resume_permalinks( $permalink, $post, $leavename ) {
  // Only affect resumes
  if ( $post->post_type !== 'resume' || empty( $permalink ) || in_array( $post->post_status, array( 'draft', 'pending', 'auto-draft' ) ) ) {
    return $permalink;
  }
  // Get the meta data to add to the permalink
  $title = get_post_meta( $post->ID, '_candidate_title', true );
  // We need data, so fallback to this if its empty
  if ( empty( $title ) ) {
    $title = 'candidate';
  }
  // Do the replacement
  $permalink = str_replace( '%candidate_title%', sanitize_title( $title ), $permalink );
  return $permalink;
}
add_filter( 'post_type_link', 'resume_permalinks', 10, 3 );




//Adding a salary field for jobs

//Add the field to the frontend
add_filter( 'submit_job_form_fields', 'frontend_add_salary_field' );

function frontend_add_salary_field( $fields ) {
  $fields['job']['job_salary'] = array(
    'label'       => __( 'Mức lương', 'virtue' ),
    'type'        => 'number',
    'placeholder' => 'vd: 5000000',
	//'description' => 'Mức lương bạn có thể trả, ghi số 0 nếu thương lượng',
	//'required'    => true,
    'priority'    => 2.5
  );
  return $fields;
}

//Add the field to admin
add_filter( 'job_manager_job_listing_data_fields', 'admin_add_salary_field' );

function admin_add_salary_field( $fields ) {
  $fields['_job_salary'] = array(
    'label'       => __( 'Salary', 'virtue' ),
    'type'        => 'text',
    'placeholder' => 'e.g. 2000 (zero if negotiable)',
    'description' => '',
	'required'    => true,
    'priority'    => 2.5
  );
  return $fields;
}

//Display "Salary" on the single job page
add_action( 'single_job_listing_meta_end', 'display_job_salary_data' );

function display_job_salary_data() {
  global $post;

  $salary = get_post_meta( $post->ID, '_job_salary', true );

  if ( $salary ) {
    echo '<li style="margin-right:0;padding-right:0;"><span class="icon-money"></span> ' . __( 'Lương: ' ) . number_format($salary,0,",",".")  . '</li>';
  }
}

//Adding a currency field for jobs

//Add the field to the frontend
add_filter( 'submit_job_form_fields', 'frontend_add_currency_field' );

function frontend_add_currency_field( $fields ) {
  $fields['job']['job_currency'] = array(
    'label'       => '',
    'type'        => 'select',
    'options' 	  => ['...','VND','USD'],
    'required'    => true,
    'default'	  => 1,
    'placeholder' => '',
    'priority'    => 2
  );
  return $fields;
}

//Add the field to admin
add_filter( 'job_manager_job_listing_data_fields', 'admin_add_currency_field' );

function admin_add_currency_field( $fields ) {
  $fields['_job_currency'] = array(
    'label'       => __( 'Currency', 'virtue' ),
    'type'        => 'select',
    'options' 	  => ['...','VND','USD'],
    'default'	  => 1,
    'placeholder' => '',
    'description' => '',
    'priority'    => 2
  );
  return $fields;
}

//Display "currency" on the single job page
add_action( 'single_job_listing_meta_end', 'display_job_currency_data' );

function display_job_currency_data() {
  global $post;

  $currency = get_post_meta( $post->ID, '_job_currency', true );
  $salary = get_post_meta( $post->ID, '_job_salary', true );

  if ( $salary ) {
	  	if ( $currency == 1) {
	    	echo '<li>đồng</li>';		  
		 } elseif ( $currency == 2) {
		  	echo '<li>USD</li>';
		 }
  } 
  else echo '<li>' . __( '<span class="icon-money"></span> Lương ' ) . __( 'thoả thuận' ) . '</li>';  
}

/**
 * Add custom taxonomies
 *
 * Additional custom taxonomies can be defined here
 * http://codex.wordpress.org/Function_Reference/register_taxonomy
 */
function add_custom_taxonomies() {
  // Add new "Locations" taxonomy to Posts
  register_taxonomy('job_work_location', 'job_listing', array(
    // Hierarchical taxonomy (like categories)
    'hierarchical' => true,
    // This array of options controls the labels displayed in the WordPress Admin UI
    'labels' => array(
      'name' => _x( 'Work Locations', 'taxonomy general name' ),
      'singular_name' => _x( 'Location', 'taxonomy singular name' ),
      'search_items' =>  __( 'Search Locations' ),
      'all_items' => __( 'All Locations' ),
      'parent_item' => __( 'Parent Location' ),
      'parent_item_colon' => __( 'Parent Location:' ),
      'edit_item' => __( 'Edit Location' ),
      'update_item' => __( 'Update Location' ),
      'add_new_item' => __( 'Add New Location' ),
      'new_item_name' => __( 'New Location Name' ),
      'menu_name' => __( 'Locations' ),
    ),
    // Control the slugs used for this taxonomy
    'rewrite' => array(
      'slug' => 'locations', // This controls the base slug that will display before each term
      'with_front' => false, // Don't display the category base before "/locations/"
      'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
    ),
  ));
}
//add_action( 'init', 'add_custom_taxonomies', 0 );

//Adding a work location field for jobs

//Add the field to the frontend
//add_filter( 'submit_job_form_fields', 'frontend_add_work_location_field' );

function frontend_add_work_location_field( $fields ) {
  $fields['job']['work_location'] = array(
    'label'       => __( 'Nơi làm việc', 'virtue' ),
	'type'        => 'term-multiselect',
	'required'    => true,
	'placeholder' => __( 'Chọn tỉnh/ thành phố', 'virtue' ),
	'priority'    => 1.5,
	'default'     => '',
	'taxonomy'    => 'job_work_location'
  );
  return $fields;
}

//Display "work location" on the single job page
//add_action( 'single_job_listing_meta_end', 'display_work_location_data' );

function display_work_location_data() {
  global $post;

  $term_list = get_the_term_list($post->ID, 'job_work_location', '<li><span class="icon-location-arrow"></span> Nơi làm việc: ',', ','</li>');
  if ($term_list ) 
	echo $term_list ;
}

function display_work_location($jobid) {

  $term_list = get_the_term_list($jobid, 'job_work_location', '',', ','');
  if ($term_list ) echo strip_tags($term_list) ;
}


/**
 * the_job_location function.
 * @param  boolean $map_link whether or not to link to the map on google maps
 * @return [type]
 */
function the_job_address( $map_link = true, $post = null ) {
	$location = get_the_job_address( $post );

	if ( $location ) {
		if ( $map_link )
			echo apply_filters( 'the_job_location_map_link', '<a class="google_map_link" href="http://maps.google.com/maps?q=' . urlencode( $location ) . '&zoom=14&size=512x512&maptype=roadmap&sensor=false" target="_blank">' . $location . '</a>', $location, $post );
		else
			echo $location;
	} else {
		//echo apply_filters( 'the_job_location_anywhere_text', __( 'Anywhere', 'wp-job-manager' ) );
		echo '';
	}
}

/**
 * get_the_job_location function.
 *
 * @access public
 * @param mixed $post (default: null)
 * @return void
 */
function get_the_job_address( $post = null ) {
	$post = get_post( $post );
	if ( $post->post_type !== 'job_listing' )
		return;

	return apply_filters( 'the_job_address', $post->_job_location, $post );
}

//Add Tags to Custom Post Type
//add_action( 'init', 'create_tag_taxonomies', 0 );

//create two taxonomies, genres and tags for the post type "tag"
function create_tag_taxonomies() 
{
  // Add new taxonomy, NOT hierarchical (like tags)
  $labels = array(
    'name' => _x( 'Tags', 'taxonomy general name' ),
    'singular_name' => _x( 'Tag', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Tags' ),
    'popular_items' => __( 'Popular Tags' ),
    'all_items' => __( 'All Tags' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Tag' ), 
    'update_item' => __( 'Update Tag' ),
    'add_new_item' => __( 'Add New Tag' ),
    'new_item_name' => __( 'New Tag Name' ),
    'separate_items_with_commas' => __( 'Separate tags with commas' ),
    'add_or_remove_items' => __( 'Add or remove tags' ),
    'choose_from_most_used' => __( 'Choose from the most used tags' ),
    'menu_name' => __( 'Tags' ),
  ); 

  register_taxonomy('job_listing_tag','job_listing',array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'tag' ),
  ));
}

//Adding a tag field for jobs

//Add the field to the frontend
//add_filter( 'submit_job_form_fields', 'frontend_add_tag_field' );

function frontend_add_tag_field( $fields ) {
  $fields['job']['job_tag'] = array(
    'label'       => __( 'You Should Be Best At', 'virtue' ),
	'type'        => 'text',
	'placeholder' => __( 'Nhập vài kỹ năng yêu cầu, phân cách bằng dấu phẩy (,)...', 'virtue' ),
	'priority'    => 5.5,
	'default'     => ''
  );
  return $fields;
}

//Gets a formatted list of job tags for a post ID
function get_job_tag_list( $job_id ) {
	$terms = get_the_term_list( $job_id, 'job_listing_tag', '<li class="tag">', apply_filters( 'virtue_tag_list_sep', '</li><li class="tag">' ), '</li>' );
	/*if ( ! apply_filters( 'enable_job_tag_archives', get_option( 'virtue_enable_tag_archive' ) ) )
		$terms = strip_tags( $terms );*/
	return $terms;
}

function display_categories_data() {
  global $post;
  $term_list = get_the_term_list($post->ID, 'job_listing_category', 'Ngành nghề: ',', ','');
  if ($term_list ) 
	echo $term_list ;
}	

//Display "Tags" on the single job page
function display_job_tags_data() {
  global $post;
  $tags = get_job_tag_list( $post->ID );
  if ( $tags ) {   
	echo '<div class="skills"><h3>Bạn nên có kỹ năng</h3></div>
	<ul class="tags">'.$tags.'</ul>';
  }
}

//Adding a job expires field for jobs

//Add the field to the frontend
add_filter( 'submit_job_form_fields', 'frontend_add_expires_field' );

function frontend_add_expires_field( $fields ) {
  $fields['job']['job_deadline'] = array(
    'label'       => __( 'Hạn nộp hồ sơ', 'virtue' ),
    'type'        => 'text',
    'required'    => true,
    'placeholder' => 'dd/mm/yyyy',
    'priority'    => 8
  );
  return $fields;
}

//Add the field to admin
add_filter( 'job_manager_job_listing_data_fields', 'admin_add_expires_field' );

function admin_add_expires_field( $fields ) {
  $fields['_job_deadline'] = array(
    'label'       => __( 'Deadline', 'virtue' ),
    'type'        => 'text',
    'placeholder' => 'dd/mm/yyyy',
    'description' => '',
    'priority'    => 2.5
  );
  return $fields;
}

//Adding a Preferred Language field for jobs

//Add the field to the frontend
add_filter( 'submit_job_form_fields', 'frontend_add_language_field' );

function frontend_add_language_field( $fields ) {
  $fields['job']['job_language'] = array(
    'label'       => __( 'Ngôn ngữ trình bày hồ sơ', 'virtue' ),
    'type'        => 'select',
    'options' 	  => ['...','Tiếng Việt','Tiếng Anh','Bất kỳ'],
    'required'    => true,
    'default'	  => 3,
    'placeholder' => '',
    'priority'    => 7.5
  );
  return $fields;
}

//Add the field to admin
add_filter( 'job_manager_job_listing_data_fields', 'admin_add_language_field' );

function admin_add_language_field( $fields ) {
  $fields['_job_language'] = array(
    'label'       => __( 'Preferred Language', 'virtue' ),
    'type'        => 'select',
    'options' 	  => ['...','Tiếng Việt','Tiếng Anh','Bất kỳ'],
    'default'	  => 1,
    'placeholder' => '',
    'description' => '',
    'priority'    => 3.5
  );
  return $fields;
}

//Adding a Job level field for jobs

//Add the field to the frontend
add_filter( 'submit_job_form_fields', 'frontend_add_level_field' );

function frontend_add_level_field( $fields ) {
  $fields['job']['job_level'] = array(
    'label'       => __( 'Cấp bậc', 'virtue' ),
    'type'        => 'select',
    'options' 	  => ['...','Mới tốt nghiệp/Thực tập sinh','Nhân viên','Trưởng nhóm/Giám sát','Trưởng phòng','Phó giám đốc','Giám đốc','Tổng giám đốc điều hành','Phó chủ tịch','Chủ tịch'],
    'required'    => true,
    'default'	  => 2,
    'placeholder' => '',
    'priority'    => 5
  );
  return $fields;
}

//Add the field to admin
add_filter( 'job_manager_job_listing_data_fields', 'admin_add_level_field' );

function admin_add_level_field( $fields ) {
  $fields['_job_level'] = array(
    'label'       => __( 'Job level', 'virtue' ),
    'type'        => 'select',
    'options' 	  => ['...','Mới tốt nghiệp/Thực tập sinh','Nhân viên','Trưởng nhóm/Giám sát','Trưởng phòng','Phó giám đốc','Giám đốc','Tổng giám đốc điều hành','Phó chủ tịch','Chủ tịch'],
    'default'	  => 2,
    'placeholder' => '',
    'description' => '',
    'priority'    => 2.5
  );
  return $fields;
}

//Adding a Job Times field for jobs

//Add the field to the frontend
add_filter( 'submit_job_form_fields', 'frontend_add_times_field' );

function frontend_add_times_field( $fields ) {
  $fields['job']['job_times'] = array(
    'label'       => __( 'Hình thức làm việc', 'virtue' ),
    'type'        => 'select',
    'options' 	  => ['...','Toàn thời gian','Bán thời gian','Freelance','Thời vụ','Thực tập'],
    'required'    => true,
    'default'	  => 1,
    'placeholder' => '',
    'priority'    => 5.5
  );
  return $fields;
}

//Add the field to admin
add_filter( 'job_manager_job_listing_data_fields', 'admin_add_times_field' );

function admin_add_times_field( $fields ) {
  $fields['_job_times'] = array(
    'label'       => __( 'Job Times', 'virtue' ),
    'type'        => 'select',
    'options' 	  => ['...','Toàn thời gian','Bán thời gian','Freelance','Thời vụ','Thực tập'],
    'default'	  => 2,
    'placeholder' => '',
    'description' => '',
    'priority'    => 2.5
  );
  return $fields;
}

//Adding a contact name field for jobs

//Add the field to the frontend
add_filter( 'submit_job_form_fields', 'frontend_add_contact_name_field' );

function frontend_add_contact_name_field( $fields ) {
  $fields['job']['job_contact_name'] = array(
    'label'       => __( 'Người liên hệ', 'virtue' ),
    'type'        => 'text',
    'required'    => true,
    'placeholder' => '',
    'priority'    => 6.5
  );
  return $fields;
}

//Add the field to admin
add_filter( 'job_manager_job_listing_data_fields', 'admin_add_contact_name_field' );

function admin_add_contact_name_field( $fields ) {
  $fields['_job_contact_name'] = array(
    'label'       => __( 'Contact name', 'virtue' ),
    'type'        => 'text',
    'placeholder' => '',
    'description' => '',
    'priority'    => 1.5
  );
  return $fields;
}

//Adding a contact phone field for jobs

//Add the field to the frontend
add_filter( 'submit_job_form_fields', 'frontend_add_contact_phone_field' );

function frontend_add_contact_phone_field( $fields ) {
  $fields['job']['job_contact_phone'] = array(
    'label'       => __( 'Điện thoại liên hệ', 'virtue' ),
    'type'        => 'text',
    'required'    => true,
    'placeholder' => '',
    'priority'    => 6.5
  );
  return $fields;
}

//Add the field to admin
add_filter( 'job_manager_job_listing_data_fields', 'admin_add_contact_phone_field' );

function admin_add_contact_phone_field( $fields ) {
  $fields['_job_contact_phone'] = array(
    'label'       => __( 'Contact phone', 'virtue' ),
    'type'        => 'text',
    'placeholder' => '',
    'description' => '',
    'priority'    => 1.5
  );
  return $fields;
}

//Add the field to the frontend
add_filter( 'submit_job_form_fields', 'frontend_company_info_field' );

function frontend_company_info_field( $fields ) {
  $fields['company']['company_info'] = array(
    'label'       => __( 'Sơ lược về Công ty', 'virtue' ),
    'type'        => 'textarea',
    'required'    => true,
    'priority'    => 3.5
  );
  return $fields;
}

//Add the field to admin
add_filter( 'job_manager_job_listing_data_fields', 'admin_company_info_field' );

function admin_company_info_field( $fields ) {
  $fields['_company_info'] = array(
    'label'       => __( 'Sơ lược về Công ty', 'virtue' ),
    'type'        => 'textarea',
    'priority'    => 5.5
  );
  return $fields;
}


/**
 * Displays job meta data on the single job page
 */
function job_listing_meta_display() {
	get_job_manager_template( 'content-single-job_listing-meta.php', array() );
}
add_action( 'single_job_listing_start', 'job_listing_meta_display', 20 );

/**
 * Displays job company data on the single job page
 */
function job_listing_company_display() {
	get_job_manager_template( 'content-single-job_listing-company.php', array() );
}
add_action( 'single_job_listing_end', 'job_listing_company_display', 30 );

// WordPress - Store timestamp of a user's last login as user meta
function user_last_login( $user_login, $user ){
    update_user_meta( $user->ID, '_last_login', current_time('mysql') );
}
add_action( 'wp_login', 'user_last_login', 10, 2 );

//function for getting the last login
function get_last_login($user_id) {
   $last_login = get_user_meta($user_id, '_last_login', true);
 
   //picking up wordpress date time format
   $date_format = get_option('date_format') . ' ' . get_option('time_format');
 
   //converting the login time to wordpress format
   $the_last_login = mysql2date($date_format, $last_login, false);
 
   //finally return the value
   return $the_last_login;
}

//Login with email
function login_with_email_address($username) {
  $user = get_user_by('email',$username);
  if(!empty($user->user_login))
    $username = $user->user_login;
  return $username;
}
add_action('wp_authenticate','login_with_email_address');

//Change Username to Username / Email
function change_username_wps_text($text){
       if(in_array($GLOBALS['pagenow'], array('wp-login.php'))){
         if ($text == 'Username'){$text = 'Username / Email';}
            }
                return $text;
         }
add_filter( 'gettext', 'change_username_wps_text' );

//Track post views
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 lượt xem";
    }
    return $count.' lượt xem';
}
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

//Get JOB custom post type categories
function get_the_job_category_bytax( $id ) {
    $terms = get_the_terms($id, 'job_listing_category' );
  if ($terms && ! is_wp_error($terms)) :
  $term_slugs_arr = array();
  foreach ($terms as $term) {
      $term_slugs_arr[] = $term->name;
  }
  $terms_slug_str = join( ", ", $term_slugs_arr);
  endif;
return $terms_slug_str;
}

//Get RESUME custom post type categories
function get_the_resume_category_bytax( $id ) {
    $terms_resume = get_the_terms($id, 'resume_category' );
  if ($terms_resume && ! is_wp_error($terms_resume)) :
  $term_slugs_arr = array();
  foreach ($terms_resume as $term) {
      $term_slugs_arr[] = $term->name;
  }
  $terms_slug_str = join( ", ", $term_slugs_arr);
  endif;
return $terms_slug_str;
}

/**
 * Based on wp_dropdown_categories, with the exception of supporting multiple selected categories.
 * @see  wp_dropdown_categories
 */
function job_manager_dropdown_location( $args = '' ) {
	$defaults = array(
		'orderby'         => 'id',
		'order'           => 'ASC',
		'show_count'      => 0,
		'hide_empty'      => 1,
		'child_of'        => 0,
		'exclude'         => '',
		'echo'            => 1,
		'selected'        => 0,
		'hierarchical'    => 0,
		'name'            => 'name',
		'id'              => '',
		'class'           => 'job-manager-location-dropdown ' . ( is_rtl() ? 'chosen-rtl' : '' ),
		'depth'           => 0,
		'taxonomy'        => 'job_work_location',
		'value'           => 'id',
		'multiple'        => true,
		'show_option_all' => false,
		'placeholder'     => __( 'Choose a location&hellip;', 'virtue' )
	);

	$r = wp_parse_args( $args, $defaults );

	if ( ! isset( $r['pad_counts'] ) && $r['show_count'] && $r['hierarchical'] ) {
		$r['pad_counts'] = true;
	}

	extract( $r );

	// Store in a transient to help sites with many cats
	$categories_hash = 'jm_cats_' . md5( json_encode( $r ) . WP_Job_Manager_Cache_Helper::get_transient_version( 'jm_get_' . $r['taxonomy'] ) );
	$categories      = get_transient( $categories_hash );

	if ( empty( $categories ) ) {
		$categories = get_terms( $taxonomy, array(
			'orderby'         => $r['orderby'],
			'order'           => $r['order'],
			'hide_empty'      => $r['hide_empty'],
			'child_of'        => $r['child_of'],
			'exclude'         => $r['exclude'],
			'hierarchical'    => $r['hierarchical']
		) );
		set_transient( $categories_hash, $categories, DAY_IN_SECONDS * 30 );
	}

	$name       = esc_attr( $name );
	$class      = esc_attr( $class );
	$id         = $id ? esc_attr( $id ) : $name;

	$output = "<select name='{$name}[]' id='$id' class='$class' " . ( $multiple ? "multiple='multiple'" : '' ) . " data-placeholder='{$placeholder}'>\n";

	if ( $show_option_all ) {
		$output .= '<option value="">' . $show_option_all . '</option>';
	}

	if ( ! empty( $categories ) ) {
		include_once( JOB_MANAGER_PLUGIN_DIR . '/includes/class-wp-job-manager-category-walker.php' );

		$walker = new WP_Job_Manager_Category_Walker;

		if ( $hierarchical ) {
			$depth = $r['depth'];  // Walk the full depth.
		} else {
			$depth = -1; // Flat.
		}

		$output .= $walker->walk( $categories, $depth, $r );
	}

	$output .= "</select>\n";

	if ( $echo ) {
		echo $output;
	}

	return $output;
}

//Adding a Salary Filter to the Job Search Form 
/**
 * This can either be done with a filter (below) or the field can be added directly to the job-filters.php template file!
 *
 * job-manager-filter class handling was added in v1.23.6
 */
add_action( 'job_manager_job_filters_search_jobs_end', 'filter_by_salary_field' );
function filter_by_salary_field() {
	?>
	<div class="search_salary">
		<label for="search_salary"><?php _e( 'Salary', 'virtue' ); ?></label>
		<select name="filter_by_salary" class="job-manager-filter">
			<option value=""><?php _e( 'Mức lương mong muốn', 'virtue' ); ?></option>
			<option value="1"><?php _e( 'Thỏa thuận', 'virtue' ); ?></option>
			<option value="1000000-3000000"><?php _e( 'Từ 1 - 3 triệu đồng', 'virtue' ); ?></option>
			<option value="3000000-5000000"><?php _e( 'Từ 3 - 5 triệu đồng', 'virtue' ); ?></option>
			<option value="5000000-7000000"><?php _e( 'Từ 5 - 7 triệu đồng', 'virtue' ); ?></option>
			<option value="6000000-8000000"><?php _e( 'Từ 6 - 8 triệu đồng', 'virtue' ); ?></option>
			<option value="8000000-10000000"><?php _e( 'Từ 8 - 10 triệu đồng', 'virtue' ); ?></option>
			<option value="10000000-15000000"><?php _e( 'Từ 10 - 15 triệu đồng', 'virtue' ); ?></option>
			<option value="15000000-29000000"><?php _e( 'Từ 15 - 20 triệu đồng', 'virtue' ); ?></option>
			<option value="over20000000"><?php _e( 'Trên 20 triệu đồng', 'virtue' ); ?></option>
			<option value="100-200"><?php _e( 'Từ 100 - 200 USD', 'virtue' ); ?></option>
			<option value="200-400"><?php _e( 'Từ 200 - 400 USD', 'virtue' ); ?></option>
			<option value="400-600"><?php _e( 'Từ 400 - 600 USD', 'virtue' ); ?></option>
			<option value="600-800"><?php _e( 'Từ 600 - 800 USD', 'virtue' ); ?></option>
			<option value="800-1000"><?php _e( 'Từ 800 - 1.000 USD', 'virtue' ); ?></option>
			<option value="1000-1500"><?php _e( 'Từ 1.000 - 1.500 USD', 'virtue' ); ?></option>			
			<option value="1500-3000"><?php _e( 'Từ 1.500 - 3.000 USD', 'virtue' ); ?></option>
			<option value="3000-5000"><?php _e( 'Từ 3.000 - 5.000 USD', 'virtue' ); ?></option>
			<option value="5000-10000"><?php _e( 'Từ 5.000 - 10.000 USD', 'virtue' ); ?></option>
		</select>
	</div>
	<?php
}
/**
 * This code gets your posted field and modifies the job search query
 */
add_filter( 'job_manager_get_listings', 'filter_by_salary_field_query_args', 10, 2 );
function filter_by_salary_field_query_args( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
		// If this is set, we are filtering by salary
		if ( ! empty( $form_data['filter_by_salary'] ) ) {
			$selected_range = sanitize_text_field( $form_data['filter_by_salary'] );
			switch ( $selected_range ) {
				case '1' :
					$query_args['meta_query'][] = array(
						'key'     => '_job_salary',
						'value'   => '0',
						'compare' => '=',
						'type'    => 'NUMERIC'
					);
				break;
				case 'over20000000' :
					$query_args['meta_query'][] = array(
						'key'     => '_job_salary',
						'value'   => '20000000',
						'compare' => '>=',
						'type'    => 'NUMERIC'
					);
				break;				
				default :
					$query_args['meta_query'][] = array(
						'key'     => '_job_salary',
						'value'   => array_map( 'absint', explode( '-', $selected_range ) ),
						'compare' => 'BETWEEN',
						'type'    => 'NUMERIC'
					);
				break;
			}
			// This will show the 'reset' link
			add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );
		}
	}
	return $query_args;
}
add_filter( 'job_manager_get_listings_custom_filter_text', 'apply_salary_filter_text' ,10 , 1 );
function apply_salary_filter_text( $text ) {
    $params = array();
    parse_str( $_POST['form_data'], $params );

    $salary = $params['filter_by_salary'] ;

    if ($salary) {
      $lev_arr = array(
        '1'=>'thỏa thuận',
        '1000000-3000000'=>'từ 1 - 3 triệu đồng',
        '3000000-5000000'=>'từ 3 - 5 triệu đồng',
        '5000000-7000000'=>'từ 5 - 7 triệu đồng',
        '6000000-8000000'=>'từ 6 - 8 triệu đồng',
        '8000000-10000000'=>'từ 8 - 10 triệu đồng',
        '10000000-15000000'=>'từ 10 - 15 triệu đồng',
        '15000000-29000000'=>'từ 15 - 20 triệu đồng',
        'over20000000'=>'trên 20 triệu đồng',
        '100-200'=>'từ 100 - 200 USD',
        '200-400'=>'từ 200 - 400 USD',
        '400-600'=>'từ 400 - 600 USD',
        '600-800'=>'từ 600 - 800 USD',
        '800-1000'=>'từ 800 - 1.000 USD',
        '1000-1500'=>'từ 1.000 - 1.500 USD',   
        '1500-3000'=>'từ 1.500 - 3.000 USD',
        '3000-5000'=>'từ 3.000 - 5.000 USD',
        '5000-10000'=>'từ 5.000 - 10.000 USD'
      );

      $text .= ' ' . __( 'lương ', 'wp-job-manager-tags' ) .   $lev_arr[$salary];

      return $text;
    }

    return $text;
  }

/**
 * This can either be done with a filter (below) or the field can be added directly to the job-filters.php template file!
 *
 * job-manager-filter class handling was added in v1.23.6
 */
add_action( 'job_manager_job_filters_search_jobs_end', 'filter_by_level_field' );
function filter_by_level_field() { ?>
<div class="search_level">
		<label for="search_level"><?php _e( 'Level', 'virtue' ); ?></label>
		<select name="filter_by_level" class="job-manager-filter">
			<option value=""><?php _e( 'Cấp bậc - chức vụ', 'virtue' ); ?></option>
			<option value="1"><?php _e( 'Mới tốt nghiệp/Thực tập sinh', 'virtue' ); ?></option>
			<option value="2"><?php _e( 'Nhân viên', 'virtue' ); ?></option>
			<option value="3"><?php _e( 'Trưởng nhóm/Giám sát', 'virtue' ); ?></option>
			<option value="4"><?php _e( 'Trưởng phòng', 'virtue' ); ?></option>
			<option value="5"><?php _e( 'Phó giám đốc', 'virtue' ); ?></option>
			<option value="6"><?php _e( 'Giám đốc', 'virtue' ); ?></option>
			<option value="7"><?php _e( 'Tổng giám đốc điều hành', 'virtue' ); ?></option>
			<option value="8"><?php _e( 'Phó chủ tịch', 'virtue' ); ?></option>
			<option value="9"><?php _e( 'Chủ tịch', 'virtue' ); ?></option>			
		</select>
	</div>
	<?php
}
/**
 * This code gets your posted field and modifies the job search query
 */
add_filter( 'job_manager_get_listings', 'filter_by_level_field_query_args', 10, 2 );
function filter_by_level_field_query_args( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
		// If this is set, we are filtering by salary
		if ( ! empty( $form_data['filter_by_level'] ) ) {
			$selected_range = sanitize_text_field( $form_data['filter_by_level'] );
			switch ( $selected_range ) {
				case '1' :
					$query_args['meta_query'][] = array(
						'key'     => '_job_level',
						'value'   => '1',
						'compare' => '=',
						'type'    => 'NUMERIC'
					);
				break;
				case '2' :
					$query_args['meta_query'][] = array(
						'key'     => '_job_level',
						'value'   => '2',
						'compare' => '=',
						'type'    => 'NUMERIC'
					);
				break;
				case '3' :
					$query_args['meta_query'][] = array(
						'key'     => '_job_level',
						'value'   => '3',
						'compare' => '=',
						'type'    => 'NUMERIC'
					);
				break;
				case '4' :
					$query_args['meta_query'][] = array(
						'key'     => '_job_level',
						'value'   => '4',
						'compare' => '=',
						'type'    => 'NUMERIC'
					);
				break;
				case '5' :
					$query_args['meta_query'][] = array(
						'key'     => '_job_level',
						'value'   => '5',
						'compare' => '=',
						'type'    => 'NUMERIC'
					);
				break;
				case '6' :
					$query_args['meta_query'][] = array(
						'key'     => '_job_level',
						'value'   => '6',
						'compare' => '=',
						'type'    => 'NUMERIC'
					);
				break;				
				case '7' :
					$query_args['meta_query'][] = array(
						'key'     => '_job_level',
						'value'   => '7',
						'compare' => '=',
						'type'    => 'NUMERIC'
					);
				break;				
				case '8' :
					$query_args['meta_query'][] = array(
						'key'     => '_job_level',
						'value'   => '8',
						'compare' => '=',
						'type'    => 'NUMERIC'
					);
				break;				
				case '9' :
					$query_args['meta_query'][] = array(
						'key'     => '_job_level',
						'value'   => '9',
						'compare' => '=',
						'type'    => 'NUMERIC'
					);
				break;					
			}
			// This will show the 'reset' link
			add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );
		}
	}
	return $query_args;
}

add_filter( 'job_manager_get_listings_custom_filter_text', 'apply_level_filter_text' ,10 , 1 );
function apply_level_filter_text( $text ) {
    $params = array();
    parse_str( $_POST['form_data'], $params );

    $level = $params['filter_by_level'] ;
    if ($level) {
      $lev_arr = array(
          1=>'Mới tốt nghiệp/Thực tập sinh',
          2=>'Nhân viên',
          3=>'Trưởng nhóm/Giám sát',
          4=>'Trưởng phòng',
          5=>'Phó giám đốc',
          6=>'Giám đốc',
          7=>'Tổng giám đốc điều hành',
          8=>'Phó chủ tịch',
          9=>'Chủ tịch'
        );

      $text .= ' ' . __( 'vị trí ', 'wp-job-manager-tags' ) .   $lev_arr[$level];

      return $text;
    }
    return $text;
}

/**
 * This can either be done with a filter (below) or the field can be added directly to the job-filters.php template file!
 *
 * job-manager-filter class handling was added in v1.23.6
 */
add_action( 'job_manager_job_filters_search_jobs_end', 'filter_by_times_field' );
function filter_by_times_field() { ?>
<div class="search_level">
		<label for="search_level"><?php _e( 'Job Times', 'virtue' ); ?></label>
		<select name="filter_by_times" class="job-manager-filter">
			<option value=""><?php _e( 'Hình thức làm việc', 'virtue' ); ?></option>
			<option value="1"><?php _e( 'Toàn thời gian', 'virtue' ); ?></option>
			<option value="2"><?php _e( 'Bán thời gian', 'virtue' ); ?></option>
			<option value="3"><?php _e( 'Freelance', 'virtue' ); ?></option>
			<option value="4"><?php _e( 'Thời vụ', 'virtue' ); ?></option>
			<option value="5"><?php _e( 'Thực tập', 'virtue' ); ?></option>					
		</select>
	</div>
	<?php
}
/**
 * This code gets your posted field and modifies the job search query
 */
add_filter( 'job_manager_get_listings', 'filter_by_times_field_query_args', 10, 2 );
function filter_by_times_field_query_args( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
		// If this is set, we are filtering by salary
		if ( ! empty( $form_data['filter_by_times'] ) ) {
			$selected_range = sanitize_text_field( $form_data['filter_by_times'] );
			switch ( $selected_range ) {
				case '1' :
					$query_args['meta_query'][] = array(
						'key'     => '_job_times',
						'value'   => '1',
						'compare' => '=',
						'type'    => 'NUMERIC'
					);
				break;
				case '2' :
					$query_args['meta_query'][] = array(
						'key'     => '_job_times',
						'value'   => '2',
						'compare' => '=',
						'type'    => 'NUMERIC'
					);
				break;
				case '3' :
					$query_args['meta_query'][] = array(
						'key'     => '_job_times',
						'value'   => '3',
						'compare' => '=',
						'type'    => 'NUMERIC'
					);
				break;
				case '4' :
					$query_args['meta_query'][] = array(
						'key'     => '_job_times',
						'value'   => '4',
						'compare' => '=',
						'type'    => 'NUMERIC'
					);
				break;
				case '5' :
					$query_args['meta_query'][] = array(
						'key'     => '_job_times',
						'value'   => '5',
						'compare' => '=',
						'type'    => 'NUMERIC'
					);
				break;
									
			}
			// This will show the 'reset' link
			add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );
		}
	}
	return $query_args;
}

add_filter( 'job_manager_get_listings_custom_filter_text', 'apply_times_filter_text' ,10 , 1 );
function apply_times_filter_text( $text ) {
    $params = array();
    parse_str( $_POST['form_data'], $params );

    $times = $params['filter_by_times'] ;
    if ($times) {
      $lev_arr = array(
          1=>'toàn thời gian',
          2=>'bán thời gian',
          3=>'freelance',
          4=>'thời vụ',
          5=>'thực tập'          
        );

      $text .= ' ' . __( 'hình thức làm việc ', 'wp-job-manager-tags' ) .   $lev_arr[$times];

      return $text;
    }
    return $text;
}

if ( ! function_exists( 'get_child_job_listing_categories' ) ) :
/**
 * Get ALL child categories, exclude parents
 *
 * @access public
 * @return array
 */
function get_child_job_listing_categories() {
  if ( ! get_option( 'job_manager_enable_categories' ) ) {
    return array();
  }

  // Fetch parent categories
  $fargs = array(
      'parent'  => 0,
      'type'    => 'job_listing',
      'taxonomy'=> 'job_listing_category',
      'hide_empty'    => 0
    );
  $parent_categories = get_categories( $fargs );


  foreach ( $parent_categories as $parent_category ) {
    // Fetch child categories
    $args = array(
      'parent'  => $parent_category->term_id,
      'type'    => 'job_listing',
      'taxonomy'=> 'job_listing_category',
      'hide_empty'    => 0
    );

    $categories = get_categories( $args );

    foreach ( $categories as $category ) {
      echo "<option value='".esc_attr( $category->term_id )."'>".$category->name."</option>";
    }
  }
}
endif;

if ( ! function_exists( 'get_job_listing_regions' ) ) :
/**
 * Get job categories
 *
 * @access public
 * @return array
 */
function get_job_listing_regions() {
  if ( ! get_option( 'job_manager_enable_categories' ) ) {
    return array();
  }

  return get_terms( "job_listing_region", array(
    'orderby'       => 'id',
      'order'         => 'ASC',
      'hide_empty'    => false,
  ) );
}
endif;

//Search form
function search_job_form () {
?>
<form class="job_filters filters_form" method="GET" action="<?php echo site_url();?>/jobs">
			  <div class="search_jobs">
			  
				<div class="fsearch_keywords">
					<label for="search_keywords">Từ khóa</label>
					<input type="text" name="search_keywords" id="search_keywords" class="form-control" placeholder="Nhập tiêu đề, vị trí công việc..." value="">
				</div>
				
				<div class="fsearch_categories">
          <div class="select-style">
					<label for="search_categories">Category</label>
						<select id="search_category" name="search_category" class="form-control">
						<option value="">Tất cả ngành nghề</option>
						<?php get_child_job_listing_categories();?>
					  <!--<?php foreach ( get_job_listing_categories() as $cat ) : ?>
						<option value="<?php echo esc_attr( $cat->term_id ); ?>"><?php echo esc_html( $cat->name ); ?></option>
              <?php endforeach; ?>-->
            </select>
          </div>
				</div>				
				<div class="fsearch_location">
          <div class="select-style">
					<label for="search_region">Tất cả địa điểm</label>
  					<select id="search_location" name="search_region" class="form-control">
  						<option value="">Tất cả địa điểm</option>
  						<?php foreach ( get_job_listing_regions() as $cat ) : ?>
  						<option value="<?php echo esc_attr( $cat->term_id ); ?>"><?php echo esc_html( $cat->name ); ?></option>
  						<?php endforeach; ?>
  					</select>		
          </div>	
				</div>
	
				<p class="search-btn">
				<input class="fsearch-btn button" type="submit" value="Tìm việc làm" />
				</p>
			  </div>
		</form>
<?php	
}

//Homepage Search form
function home_search_job_form () {
?>
<form method="GET" action="<?php echo site_url();?>/jobs">	
					<div class="form-group">
						<input type="text" id="search_keywords" class="form-control" name="search_keywords" placeholder="Nhập tiêu đề, vị trí công việc...">
					</div>
					<div class="form-group">
							<div class="select-style">
								<select name="search_category" id="search_category" class="form-control">
									<option value="">Tất cả ngành nghề</option>
									<?php get_child_job_listing_categories();?>
									<!--<?php foreach ( get_job_listing_categories() as $cat ) : ?>
									<option value="<?php echo esc_attr( $cat->term_id ); ?>"><?php echo esc_html( $cat->name ); ?></option>
								  <?php endforeach; ?>-->
								</select>
							</div>
						</div>
					<div class="form-group">
						<div class="select-style">
								<select  name="search_region" id="search_location" class="form-control">
									<option value="">Tất cả địa điểm</option>
									<?php foreach ( get_job_listing_regions() as $cat ) : ?>
									<option value="<?php echo esc_attr( $cat->term_id ); ?>"><?php echo esc_html( $cat->name ); ?></option>
									<?php endforeach; ?>
								</select>
							
						</div>
					</div>						
					<div class="form-group">
						<button type="submit" class="btn btn-success btn-block">Tìm việc làm</button>
					</div>					
			</form>
<?php	
}

//Dung cho trang tim viec theo nganh nghe, hien thi category cha va category con
function get_job_categories( $args = '' ) {
	$defaults = array( 'taxonomy' => 'job_listing_category', 'hide_empty'=>'0' );
	$args = wp_parse_args( $args, $defaults );

	$taxonomy = apply_filters( 'get_categories_taxonomy', $args['taxonomy'], $args );

	// Back compat
	if ( isset($args['type']) && 'link' == $args['type'] ) {
		_deprecated_argument( __FUNCTION__, '3.0', '' );
		$taxonomy = $args['taxonomy'] = 'link_category';
	}

	$categories = (array) get_terms( $taxonomy, $args );

	foreach ( array_keys( $categories ) as $k )
		_make_cat_compat( $categories[$k] );
	return $categories;
}
//Dung cho trang tim viec theo nganh nghe, hien thi link cua category
function get_job_category_link( $category ) {
	if ( ! is_object( $category ) )
		$category = (int) $category;

	$category = get_term_link( $category, 'job_listing_category' );

	if ( is_wp_error( $category ) )
		return '';

	return $category;
}

//Dung cho trang tim viec theo dia diem, hien thi link cua tax
function get_job_region_link( $category ) {
  if ( ! is_object( $category ) )
    $category = (int) $category;

  $category = get_term_link( $category, 'job_listing_region' );

  if ( is_wp_error( $category ) )
    return '';

  return $category;
}

//Build A Fully Customized WordPress Login Page
//Redirect wp-login page
function redirect_login_page() {
    $login_page  = home_url( '/dang-nhap/' );
    $page_viewed = basename($_SERVER['REQUEST_URI']);
 
    if( $page_viewed == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {
        wp_redirect($login_page);
        exit;
    }
}
add_action('init','redirect_login_page');
function login_failed() {
    $login_page  = home_url( '/dang-nhap/' );
    wp_redirect( $login_page . '?login=failed' );
    exit;
}
add_action( 'wp_login_failed', 'login_failed' );
 
function verify_username_password( $user, $username, $password ) {
    $login_page  = home_url( '/dang-nhap/' );
    if( $username == "" || $password == "" ) {
        wp_redirect( $login_page . "?login=empty" );
        exit;
    }
}
add_filter( 'authenticate', 'verify_username_password', 1, 3);
function logout_page() {
    $login_page  = home_url( '/dang-nhap/' );
    wp_redirect( $login_page . "?login=false" );
    exit;
}
add_action('wp_logout','logout_page');

function login_form() {
?>
		<form class="clearfix" action="<?php echo site_url('wp-login.php') ?>" method="post">
					<label class="grey" for="log"><?php _e('Username') ?>:</label>
					<input class="field" type="text" name="log" id="log" value="<?php echo wp_specialchars(stripslashes($user_login), 1) ?>" size="23" />
					<label class="grey" for="pwd"><?php _e('Password:') ?></label>
					<input class="field" type="password" name="pwd" id="pwd" size="23" />
	            	<label><input name="rememberme" id="rememberme" type="checkbox" checked="checked" value="forever" />&nbsp;<?php _e('Remember Me') ?></label>
        			<div class="clear"></div>
					<input type="submit" name="submit" value="<?php _e('Log In') ?>" class="bt_login" />
					<input type="hidden" name="redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>"/>
					<p class="clear" style="margin-top: 10px;"><a class="lost-pwd" href="<?php echo site_url(); ?>/lost-password/"><?php _e('Lost Password') ?></a></p>
		</form>
		<div style="border-bottom:1px solid #e9e9e9;"></div>
		<p class="clear">Bạn chưa có tài khoản đăng nhập?</p>
		<p class="clear"><a class="line_bot" href="<?php echo site_url();?>/dang-ky-tai-khoan/" rel="nofollow">Đăng ký miễn phí tại đây.</a></p>
<?php
}
// Check if the menu exists
$menu_name = 'Slide Down Menu';
$menu_exists = wp_get_nav_menu_object( $menu_name );

// If it doesn't exist, let's create it.
if( !$menu_exists){
    $menu_id = wp_create_nav_menu($menu_name);

	// Set up default menu items
    wp_update_nav_menu_item($menu_id, 0, array(
        'menu-item-title' =>  __('Home'),
        'menu-item-classes' => 'home',
        'menu-item-url' => home_url( '/' ), 
        'menu-item-status' => 'publish'));

    wp_update_nav_menu_item($menu_id, 0, array(
        'menu-item-title' =>  __('Custom Page'),
        'menu-item-url' => home_url( '/custom/' ), 
        'menu-item-status' => 'publish'));

}
register_nav_menus( array(
	'slide_down' => 'Slide Down Navigation Menu',
	'footer_column' => 'Footer Column Navigation Menu',
) );
//Build A Fully Customized WordPress Login Page

/**
 * Redirect user after successful login.
 *
 * @param string $redirect_to URL to redirect to.
 * @param string $request URL the user is coming from.
 * @param object $user Logged user's data.
 * @return string
 */

function my_login_redirect( $redirect_to, $request, $user ) {
  //is there a user to check?
  global $user;
  if ( isset( $user->roles ) && is_array( $user->roles ) ) {
    //check for admins
    if ( in_array( 'employer', $user->roles ) ) {
      // redirect them to the default place      
      return $redirect_to;
    } else {
      //return home_url();
	  return $redirect_to;
    }
  } else {
    return $redirect_to;
  }
}

add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );

/**
	 * When viewing a taxonomy archive, use the same template for all.
	 *
	 * @since Jobify 1.0
	 *
	 * @return void
	 */	
add_filter( 'taxonomy_archive', 'job_archives' );
function job_archives( $original_template ) {
	global $wp_query;
		$taxonomies = array(
			'job_listing_category',
			'job_listing_region',
			//'job_listing_type',
			//'job_listing_tag'
		);
		if ( is_tax( $taxonomies ) ) {
		return get_stylesheet_directory_uri().'/taxonomy-job_listing_category.php';
		  } else {
		return $original_template;
		}
}

/**
 * Sanitizes title, replacing whitespace and a few other characters with dashes.
 * @param string $str The title in Vietnamese.
 * @return string The Vietnamese without special character.
 */
function khongdau($str) {
			$str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
			$str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
			$str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
			$str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
			$str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
			$str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
			$str = preg_replace("/(đ)/", 'd', $str);
			$str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
			$str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
			$str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
			$str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
			$str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
			$str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
			$str = preg_replace("/(Đ)/", 'D', $str);
			//$str = str_replace(" ", "-", str_replace("&*#39;","",$str));
			return $str; 
	}

	
//Resumes
//Adding a candidate address field for jobs

//Add the field to the frontend
// Add field to admin
add_filter( 'resume_manager_resume_fields', 'wpjms_admin_resume_address_fields' );
function wpjms_admin_resume_address_fields( $fields ) {
	
	$fields['_candidate_address'] = array(
	    'label' 		=> __( 'Địa chỉ', 'virtue' ),
	    'type' 			=> 'text',
	    //'placeholder' 	=> __( 'Blue', 'job_manager' ),
	    'description'	=> '',
		'priority'    => 2
	);

	return $fields;
	
}

// Add field to frontend
add_filter( 'submit_resume_form_fields', 'wpjms_frontend_resume_address_fields' );
function wpjms_frontend_resume_address_fields( $fields ) {
	
	$fields['resume_fields']['candidate_address'] = array(
	    'label' => __( 'Địa chỉ', 'virtue' ),
	    'type' => 'text',
	    'required' => true,
	    'placeholder' => '',
		'priority'    => 3.5
	);

	return $fields;
	
}

//to add defer to loading of scripts - use defer to keep loading order
function script_tag_defer($tag, $handle) {
    if (is_admin()){
        return $tag;
    }
    if (strpos($tag, '/wp-includes/js/jquery/jquery')) {
        return $tag;
    }
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 9.') !==false) {
    return $tag;
    }
    else {
        return str_replace(' src',' defer src', $tag);
    }
}
add_filter('script_loader_tag', 'script_tag_defer',10,2);