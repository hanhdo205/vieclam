<?php
/*
Plugin Name: Job Widget by Hanh
Description: Site specific code changes for example.com
*/
/* Start Adding Functions Below this Line */

/**
 * Home: Recent Jobs
 *
 */
 /**
 * Widget Base Class
 *
 * Based on Job Manager Widget Base
 *
 * @since Jobify 1.0
 */


/**
 * Jobify Widget base
 */
class Jobify_Widget extends WP_Widget {

	public $widget_cssclass;
	public $widget_description;
	public $widget_id;
	public $widget_name;
	public $settings;
	public $control_ops;

	/**
	 * Constructor
	 */
	public function __construct() {
		$widget_ops = array(
			'classname'   => $this->widget_cssclass,
			'description' => $this->widget_description
		);

		if ( ! $this->widget_id ) {
			$this->widget_id = null;
		}

		$this->WP_Widget( $this->widget_id, $this->widget_name, $widget_ops, $this->control_ops );

		add_action( 'save_post', array( $this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array( $this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array( $this, 'flush_widget_cache' ) );
	}

	/**
	 * get_cached_widget function.
	 */
	function get_cached_widget( $args ) {
		$cache = wp_cache_get( $this->widget_id, 'widget' );

		if ( ! is_array( $cache ) )
			$cache = array();

		if ( ! isset( $args[ 'widget_id' ] ) )
			return false;

		if ( isset( $cache[ $args[ 'widget_id' ] ] ) ) {
			echo $cache[ $args[ 'widget_id' ] ];
			return true;
		}

		return false;
	}

	/**
	 * Cache the widget
	 */
	public function cache_widget( $args, $content ) {
		if ( ! isset( $args[ 'widget_id' ] ) )
			return;

		$cache[ $args[ 'widget_id' ] ] = $content;

		wp_cache_set( $this->widget_id, $cache, 'widget' );
	}

	/**
	 * Flush the cache
	 * @return [type]
	 */
	public function flush_widget_cache() {
		wp_cache_delete( $this->widget_id, 'widget' );
	}

	/**
	 * update function.
	 *
	 * @see WP_Widget->update
	 * @access public
	 * @param array $new_instance
	 * @param array $old_instance
	 * @return array
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		if ( ! $this->settings )
			return $instance;

		foreach ( $this->settings as $key => $setting ) {
			switch ( $setting[ 'type' ] ) {
				case 'textarea' :
					if ( current_user_can( 'unfiltered_html' ) )
						$instance[ $key ] = $new_instance[ $key ];
					else
						$instance[ $key ] = wp_kses_data( $new_instance[ $key ] );
				break;
				case 'number' :
					$instance[ $key ] = absint( $new_instance[ $key ] );
				break;
				case 'multicheck' :
					$instance[ $key ] = maybe_serialize( $new_instance[ $key ] );
				break;
				default :
					$instance[ $key ] = sanitize_text_field( $new_instance[ $key ] );
				break;
			}
		}

		$this->flush_widget_cache();

		return $instance;
	}

	/**
	 * form function.
	 *
	 * @see WP_Widget->form
	 * @access public
	 * @param array $instance
	 * @return void
	 */
	function form( $instance ) {

		if ( ! $this->settings )
			return;

		foreach ( $this->settings as $key => $setting ) {

			$value = isset( $instance[ $key ] ) ? $instance[ $key ] : $setting[ 'std' ];

			switch ( $setting[ 'type' ] ) {
				case 'description' :
					?>
					<p class="description"><?php echo $value; ?></p>
					<?php
				break;
				case 'text' :
					?>
					<p>
						<label for="<?php echo $this->get_field_id( $key ); ?>"><?php echo $setting[ 'label' ]; ?></label>
						<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( $key ) ); ?>" name="<?php echo $this->get_field_name( $key ); ?>" type="text" value="<?php echo esc_attr( $value ); ?>" />
					</p>
					<?php
				break;
				case 'checkbox' :
					?>
					<p>
						<label for="<?php echo $this->get_field_id( $key ); ?>">
							<input type="checkbox" id="<?php echo esc_attr( $this->get_field_id( $key ) ); ?>" name="<?php echo $this->get_field_name( $key ); ?>" value="1" <?php checked( 1, esc_attr( $value ) ); ?>/>
							<?php echo $setting[ 'label' ]; ?>
						</label>
					</p>
					<?php
				break;
				case 'multicheck' :
					$value = maybe_unserialize( $value );

					if ( ! is_array( $value ) )
						$value = array();
					?>
					<p><?php echo esc_attr( $setting[ 'label' ] ); ?></p>
					<p>
						<?php foreach ( $setting[ 'options' ] as $id => $label ) : ?>
						<label for="<?php echo esc_attr( $id ); ?>">
							<input type="checkbox" id="<?php echo esc_attr( $id ); ?>" name="<?php echo $this->get_field_name( $key ); ?>[]" value="<?php echo esc_attr( $id ); ?>" <?php if ( in_array( $id, $value ) ) : ?>checked="checked"<?php endif; ?>/>
							<?php echo esc_attr( $label ); ?><br />
						</label>
						<?php endforeach; ?>
					</p>
					<?php
				break;
				case 'select' :
					?>
					<p>
						<label for="<?php echo $this->get_field_id( $key ); ?>"><?php echo $setting[ 'label' ]; ?></label>
						<select class="widefat" id="<?php echo esc_attr( $this->get_field_id( $key ) ); ?>" name="<?php echo $this->get_field_name( $key ); ?>">
							<?php foreach ( $setting[ 'options' ] as $key => $label ) : ?>
							<option value="<?php echo esc_attr( $key ); ?>" <?php selected( $key, $value ); ?>><?php echo esc_attr( $label ); ?></option>
							<?php endforeach; ?>
						</select>
					</p>
					<?php
				break;
				case 'number' :
					?>
					<p>
						<label for="<?php echo $this->get_field_id( $key ); ?>"><?php echo $setting[ 'label' ]; ?></label>
						<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( $key ) ); ?>" name="<?php echo $this->get_field_name( $key ); ?>" type="number" step="<?php echo esc_attr( $setting[ 'step' ] ); ?>" min="<?php echo esc_attr( $setting[ 'min' ] ); ?>" max="<?php echo esc_attr( $setting[ 'max' ] ); ?>" value="<?php echo esc_attr( $value ); ?>" />
					</p>
					<?php
				break;
				case 'textarea' :
					?>
					<p>
						<label for="<?php echo $this->get_field_id( $key ); ?>"><?php echo $setting[ 'label' ]; ?></label>
						<textarea class="widefat" id="<?php echo esc_attr( $this->get_field_id( $key ) ); ?>" name="<?php echo $this->get_field_name( $key ); ?>" rows="<?php echo $setting[ 'rows' ]; ?>"><?php echo esc_html( $value ); ?></textarea>
					</p>
					<?php
				break;
				case 'colorpicker' :
						wp_enqueue_script( 'wp-color-picker' );
						wp_enqueue_style( 'wp-color-picker' );
					?>
						<p style="margin-bottom: 0;">
							<label for="<?php echo $this->get_field_id( $key ); ?>"><?php echo $setting[ 'label' ]; ?></label>
						</p>
						<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( $key ) ); ?>" name="<?php echo $this->get_field_name( $key ); ?>" data-default-color="<?php echo $value; ?>" value="<?php echo $value; ?>" />
						<script>
							jQuery(document).ready(function($){
								$( 'input[name="<?php echo esc_attr( $this->get_field_name( $key ) ); ?>"]' ).wpColorPicker();
							});
						</script>
						<p></p>
					<?php
				break;
			}
		}
	}
}

// Register and load the widget
function wpb_load_widget() {
	register_widget( 'Jobify_Widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );

class Jobify_Widget_Jobs extends Jobify_Widget {

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->widget_cssclass    = 'jobify_widget_jobs';
		$this->widget_description = __( 'Output a list of recent jobs.', 'jobify' );
		$this->widget_id          = 'jobify_widget_jobs';
		$this->widget_name        = __( 'Jobify - Home: Recent Jobs', 'jobify' );
		$this->settings           = array(
			'title' => array(
				'type'  => 'text',
				'std'   => __( 'Recent Jobs', 'jobify' ),
				'label' => __( 'Title:', 'jobify' )
			),
			'filters' => array(
				'type'  => 'checkbox',
				'std'   => 1,
				'label' => __( 'Show Filters', 'jobify' )
			),
			'number' => array(
				'type'  => 'number',
				'step'  => 1,
				'min'   => 1,
				'max'   => '',
				'std'   => 5,
				'label' => __( 'Number of jobs to show:', 'jobify' )
			),
			'spotlight' => array(
				'type'  => 'checkbox',
				'std'   => 1,
				'label' => __( 'Display the "Job Spotlight" section', 'jobify' )
			),
			'spotlight-title' => array(
				'type'  => 'text',
				'std'   => __( 'Job Spotlight', 'jobify' ),
				'label' => __( 'Spotlight Title:', 'jobify' )
			)
		);

		parent::__construct();
	}

	/**
	 * widget function.
	 *
	 * @see WP_Widget
	 * @access public
	 * @param array $args
	 * @param array $instance
	 * @return void
	 */
	function widget( $args, $instance ) {
		if ( $this->get_cached_widget( $args ) )
			return;

		ob_start();

		extract( $args );

		$title   = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base );
		$number  = $instance[ 'number' ];
		$filters = isset ( $instance[ 'filters' ] ) && '1' == $instance[ 'filters' ] ? 1 : 0;

		$has_spotlight   = $instance[ 'spotlight' ];
		$spotlight_title = isset( $instance[ 'spotlight-title' ] ) ? esc_attr( $instance[ 'spotlight-title' ] ) : __( 'Job Spotlight', 'jobify' );

		if ( $has_spotlight ) {
			$spotlight = new WP_Query( array(
				'post_type' => 'job_listing',
				'post_status' => 'publish',
				'meta_query' => array(
					array(
						'key'   => '_featured',
						'value' => 1
					)
				),
				'posts_per_page' => 1,
				'orderby' => 'rand',
				'no_found_rows' => true,
				'update_post_term_cache' => false,
				'update_post_meta_cache' => false,
			) );

			$has_spotlight = $spotlight->have_posts();
		}

		echo $before_widget;
		?>

			<div class="container">

				<div class="row">

					<div class="recent-jobs <?php if ( $filters ) : ?>filters<?php endif; ?> <?php if ( $has_spotlight ) : ?>has-spotlight col-lg-8 col-md-6 col-sm-12<?php else : ?>col-xs-12<?php endif; ?>">
						<?php
							if ( $title ) echo $before_title . $title . $after_title;
							echo do_shortcode( '[jobs show_filters=' . $filters . ' per_page=' . $number . ']' );
						?>
					</div>

					<?php if ( $has_spotlight ): ?>
					<div class="job-spotlight col-lg-4 col-md-6 col-sm-12">
						<h3 class="homepage-widget-title"><?php echo esc_attr( $spotlight_title ); ?></h3>

						<?php
							while ( $spotlight->have_posts() ) : $spotlight->the_post();
								add_filter( 'excerpt_length', array( $this, 'excerpt_length' ) );
						?>
							<?php get_template_part( 'content', 'single-job-featured' ); ?>
						<?php
								remove_filter( 'excerpt_length', array( $this, 'excerpt_length' ) );
							endwhile;
						?>
					</div>
					<?php endif; ?>

				</div>

			</div>

		<?php
		echo $after_widget;

		$content = apply_filters( $this->widget_id, ob_get_clean(), $instance, $args );

		echo $content;

		$this->cache_widget( $args, $content );
	}

	public function excerpt_length() {
		return 20;
	}
}

// Register and load the widget
function wpb_load_job_widget() {
	register_widget( 'Jobify_Widget_Jobs' );
}
add_action( 'widgets_init', 'wpb_load_job_widget' );

/**
 * Job/Resume: Actions
 *
 * @since Jobify 1.6.0
 */
class Jobify_Widget_Job_Apply extends Jobify_Widget {

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->widget_cssclass    = 'jobify_widget_job_apply';
		$this->widget_description = __( 'Display the job/resume action buttons', 'jobify' );
		$this->widget_id          = 'jobify_widget_job_apply';
		$this->widget_name        = __( 'Jobify - Job/Resume: Actions', 'jobify' );
		$this->settings           = array(
			'title' => array(
				'type'  => 'text',
				'std'   => '',
				'label' => __( 'Title:', 'jobify' )
			)
		);

		//$this->settings = jobify_rcp_subscription_selector( $this->settings );

		parent::__construct();
	}

	/**
	 * widget function.
	 *
	 * @see WP_Widget
	 * @access public
	 * @param array $args
	 * @param array $instance
	 * @return void
	 */
	function widget( $args, $instance ) {
		if ( $this->get_cached_widget( $args ) )
			return;

		ob_start();

		extract( $args );

		global $post;

		if ( 'preview' == $post->post_status ) {
			return;
		}

		$title = apply_filters( 'widget_title', isset ( $instance[ 'title' ] ) ? $instance[ 'title' ] : '', $instance, $this->id_base );

		echo $before_widget;
		?>

		<?php if ( $title ) echo $before_title . $title . $after_title; ?>

		<?php if ( 'job_listing' == get_post_type() ) : ?>

			<?php if ( ! is_position_filled() ) : ?>
				<?php get_job_manager_template( 'job-application.php' ); ?>
			<?php endif; ?>

			<?php if ( '' != get_the_company_video() ) : ?>

				<a href="#company-video" class="button view-video popup-trigger"><?php _e( 'Watch Video', 'jobify' ); ?></a>

				<div id="company-video" class="modal">
					<?php the_company_video(); ?>
				</div>

			<?php endif; ?>

		<?php else : ?>

			<?php get_job_manager_template( 'contact-details.php', array( 'post' => $post ), 'resume_manager', RESUME_MANAGER_PLUGIN_DIR . '/templates/' ); ?>

			<?php if ( '' != get_the_candidate_video() ) : ?>

				<a href="#candidate-video" class="button view-video popup-trigger"><?php _e( 'Video Resume', 'jobify' ); ?></a>

				<div id="candidate-video" class="modal">
					<?php the_candidate_video(); ?>
				</div>

			<?php endif; ?>

		<?php endif; ?>

		<?php do_action( 'jobify_widget_job_apply_after' ); ?>

		<?php
		echo $after_widget;

		$content = ob_get_clean();

		echo apply_filters( 'jobify_widget_job_apply', $content, $instance, $args );

		$this->cache_widget( $args, $content );
	}
}

// Register and load the widget
function wpb_load_apply_widget() {
	register_widget( 'Jobify_Widget_Job_Apply' );
}
add_action( 'widgets_init', 'wpb_load_apply_widget' );
/* Stop Adding Functions Below this Line */

/**
 * Job: Company Listings
 *
 * @since Jobify 1.6.0
 */
class Jobify_Widget_Job_More_Jobs extends Jobify_Widget {

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->widget_cssclass    = 'jobify_widget_job_more_jobs';
		$this->widget_description = __( 'Display a link to more jobs from the company', 'jobify' );
		$this->widget_id          = 'jobify_widget_job_more_jobs';
		$this->widget_name        = __( 'Jobify - Job: Company Listings', 'jobify' );
		$this->settings           = array(
			'title' => array(
				'type'  => 'text',
				'std'   => '',
				'label' => __( 'Title:', 'jobify' )
			)
		);

		//$this->settings = jobify_rcp_subscription_selector( $this->settings );

		parent::__construct();
	}

	/**
	 * widget function.
	 *
	 * @see WP_Widget
	 * @access public
	 * @param array $args
	 * @param array $instance
	 * @return void
	 */
	function widget( $args, $instance ) {
		if ( $this->get_cached_widget( $args ) )
			return;

		ob_start();

		extract( $args );

		if ( ! class_exists( 'Astoundify_Job_Manager_Companies' ) )
			return;

		$title       = apply_filters( 'widget_title', isset ( $instance[ 'title' ] ) ? $instance[ 'title' ] : '', $instance, $this->id_base );
		$companies   = Astoundify_Job_Manager_Companies::instance();
		$company_url = esc_url( $companies->company_url( get_the_company_name() ) );

		echo $before_widget;
		?>

		<?php if ( $title ) echo $before_title . $title . $after_title; ?>

		<a href="<?php echo $company_url; ?>" title="<?php printf( __( 'More jobs by %s', 'jobify' ), get_the_company_name() ); ?>"><i class="icon-newspaper"></i> <?php _e( 'More Jobs', 'jobify' ); ?></a>

		<?php
		echo $after_widget;

		$content = apply_filters( 'jobify_widget_job_more_jobs', ob_get_clean(), $instance, $args );

		echo $content;

		$this->cache_widget( $args, $content );
	}
}
// Register and load the widget
function wpb_load_more_job_widget() {
	register_widget( 'Jobify_Widget_Job_More_Jobs' );
}
add_action( 'widgets_init', 'wpb_load_more_job_widget' );

/**
 * Home: Jobs Search
 *
 * @since Jobify 1.7.0
 */
class Jobify_Widget_Jobs_Search extends Jobify_Widget {

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->widget_cssclass    = 'jobify_widget_jobs_search';
		$this->widget_description = __( 'Output search options to search jobs.', 'jobify' );
		$this->widget_id          = 'jobify_widget_jobs_search';
		$this->widget_name        = __( 'Jobify - Home: Jobs Search', 'jobify' );
		$this->settings           = array(
			'title' => array(
				'type'  => 'text',
				'std'   => __( 'Search Jobs', 'jobify' ),
				'label' => __( 'Title:', 'jobify' )
			)
		);

		parent::__construct();
	}

	/**
	 * widget function.
	 *
	 * @see WP_Widget
	 * @access public
	 * @param array $args
	 * @param array $instance
	 * @return void
	 */
	function widget( $args, $instance ) {
		if ( $this->get_cached_widget( $args ) )
			return;

		ob_start();

		extract( $args );

		$title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base );

		echo $before_widget;
		?>

			<div class="container">

				<?php if ( $title ) echo $before_title . $title . $after_title; ?>

				<?php do_action( 'jobify_output_job_results' ); ?>

			</div>

		<?php
		echo $after_widget;

		$content = ob_get_clean();

		echo apply_filters( 'jobify_widget_jobs_search', $content );

		$this->cache_widget( $args, $content );
	}
}
// Register and load the widget
function wpb_load_job_search_widget() {
	register_widget( 'Jobify_Widget_Jobs_Search' );
}
add_action( 'widgets_init', 'wpb_load_job_search_widget' );

/**
 * Price Table for WooCommerce Paid Listings
 *
 * Automatically populated with subscriptions.
 *
 * @since Jobify 1.0
 */
class Jobify_Widget_Price_Table_WC extends Jobify_Widget {

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->widget_cssclass    = 'jobify_widget_price_table_wc';
		$this->widget_description = __( 'Outputs Job Packages from WooCommerce', 'jobify' );
		$this->widget_id          = 'jobify_widget_price_table_wc';
		$this->widget_name        = __( 'Jobify - Home: WooCommerce Packages', 'jobify' );
		$this->settings           = array(
			'title' => array(
				'type'  => 'text',
				'std'   => __( 'Plans and Pricing', 'jobify' ),
				'label' => __( 'Title:', 'jobify' )
			),
			'orderby' => array(
				'label' => __( 'Order By:', 'jobify' ),
				'type' => 'select',
				'std'  => 'id',
				'options' => array(
					'id'   => __( 'ID', 'jobify' ),
					'title' => __( 'Title', 'jobify' ),
					'menu_order' => __( 'Menu Order', 'jobify' )
				)
			),
			'order' => array(
				'label' => __( 'Order:', 'jobify' ),
				'type' => 'select',
				'std'  => 'desc',
				'options' => array(
					'desc'   => __( 'DESC', 'jobify' ),
					'asc' => __( 'ASC', 'jobify' )
				)
			),
			'packages' => array(
				'label' => __( 'Package Type:', 'jobify' ),
				'type' => 'select',
				'std' => 'job_package',
				'options' => array(
					'job_package' => __( 'Job Packages', 'jobify' ),
					'resume_package' => __( 'Resume Packages', 'jobify' )
				)
			),
			'description' => array(
				'type'  => 'textarea',
				'rows'  => 4,
				'std'   => '',
				'label' => __( 'Description:', 'jobify' ),
			)
		);

		parent::__construct();
	}

	/**
	 * widget function.
	 *
	 * @see WP_Widget
	 * @access public
	 * @param array $args
	 * @param array $instance
	 * @return void
	 */
	function widget( $args, $instance ) {
		if ( $this->get_cached_widget( $args ) )
			return;

		ob_start();

		extract( $args );

		$title        = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base );
		$description  = $instance[ 'description' ];

		$orderby = isset( $instance[ 'orderby' ] ) ? $instance[ 'orderby' ] : 'id';
		$order = isset( $instance[ 'order' ] ) ? $instance[ 'order' ] : 'desc';
		$packages = isset( $instance[ 'packages' ] ) ? esc_attr( $instance[ 'packages' ] ) : 'job_package';

		$type = 'job_package' == $packages ? 'job_listing' : 'resume';
		$obj  = get_post_type_object( $type );

		$packages     = get_posts( array(
			'post_type'  => 'product',
			'posts_per_page' => -1,
			'tax_query'  => array(
				array(
					'taxonomy' => 'product_type',
					'field'    => 'slug',
					'terms'    => array( $packages, $packages . '_subscription' )
				)
			),
			'meta_query' => array(
				array(
					'key'     => '_visibility',
					'value'   => array( 'catalog', 'visible' ),
					'compare' => 'IN'
				)
			),
			'orderby' => $orderby,
			'order'   => $order,
			'suppress_filters' => 0
		) );

		if ( ! $packages || ! class_exists( 'WP_Job_Manager_WCPL' ) )
			return;

		$content = ob_get_clean();

		echo $before_widget;
		?>

		<div class="container">

			<?php if ( $title ) echo $before_title . $title . $after_title; ?>

			<?php if ( $description ) : ?>
				<p class="homepage-widget-description"><?php echo $description; ?></p>
			<?php endif; ?>

			<div class="row pricing-table-wrapper" data-columns>

				<?php foreach ( $packages as $key => $package ) : $product = get_product( $package ); ?>
					<div class="pricing-table-widget-wrapper">
						<div class="pricing-table-widget">
							<div class="pricing-table-widget-title">
								<?php echo get_post_field( 'post_title', $package ); ?>
							</div>

							<div class="pricing-table-widget-description">
								<h2><?php echo $product->get_price_html(); ?></h2>

								<p><span class="rcp_level_duration">
									<?php
										printf( _n( '%s for %d job', '%s for %s jobs', $product->get_limit(), 'jobify' ) . ' ', $product->get_price_html(), $product->get_limit() ? $product->get_limit() : __( 'unlimited', 'wp-job-manager-wc-paid-listings' ) );

										if ( $product->get_duration() ) {
											printf( _n( 'listed for %s day', 'listed for %s days', $product->get_duration(), 'jobify' ), $product->get_duration() );
										}
									?>
								</span></p>

								<?php echo apply_filters( 'the_content', get_post_field( 'post_content', $product->id ) ); ?>

								<p>
									<?php
										$link 	= $product->add_to_cart_url();
										$label 	= apply_filters( 'add_to_cart_text', __( 'Add to Cart', 'jobify' ) );
									?>
									<a href="<?php echo esc_url( $link ); ?>" class="button-secondary"><?php echo $label; ?></a>
								</p>
							</div>
						</div>
					</div>

				<?php endforeach; ?>

			</div>

		</div>

		<?php
		echo $after_widget;

		echo $content;

		$this->cache_widget( $args, $content );
	}
}

// Register and load the widget
function wpb_load_job_price_widget() {
	register_widget( 'Jobify_Widget_Price_Table_WC' );
}
add_action( 'widgets_init', 'wpb_load_job_price_widget' );

/**
 * Job: Level
 *
 * @since Jobify 1.6.0
 */
class Jobify_Widget_Job_Level extends Jobify_Widget {

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->widget_cssclass    = 'jobify_widget_job_level';
		$this->widget_description = __( 'Display the job\'s level', 'jobify' );
		$this->widget_id          = 'jobify_widget_job_level';
		$this->widget_name        = __( 'Jobify - Job: level', 'jobify' );
		$this->settings           = array(
			'title' => array(
				'type'  => 'text',
				'std'   => '',
				'label' => __( 'Title:', 'jobify' )
			)
		);

		//$this->settings = jobify_rcp_subscription_selector( $this->settings );

		parent::__construct();
	}

	/**
	 * widget function.
	 *
	 * @see WP_Widget
	 * @access public
	 * @param array $args
	 * @param array $instance
	 * @return void
	 */
	function widget( $args, $instance ) {
		if ( $this->get_cached_widget( $args ) )
			return;

		ob_start();

		extract( $args );

		global $post;

		$title = apply_filters( 'widget_title', isset ( $instance[ 'title' ] ) ? $instance[ 'title' ] : 'Cấp bậc', $instance, $this->id_base );
		$level = get_the_terms( $post->ID, 'job_listing_category' );

		if ( ! $level )
			return;

		echo $before_widget;
		?>

		<?php if ( $title ) echo $before_title . $title . $after_title; ?>

		<div class="job_listing-level">

			<?php the_job_level(); ?>

		</div>

		<?php
		echo $after_widget;

		$content = apply_filters( 'jobify_widget_job_level', ob_get_clean(), $instance, $args );

		echo $content;

		$this->cache_widget( $args, $content );
	}
}
// Register and load the widget
function wpb_load_job_level_widget() {
	register_widget( 'Jobify_Widget_Job_Level' );
}
add_action( 'widgets_init', 'wpb_load_job_level_widget' );

/**
 * Job: Language
 *
 * @since Jobify 1.6.0
 */
class Jobify_Widget_Job_Language extends Jobify_Widget {

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->widget_cssclass    = 'jobify_widget_job_language';
		$this->widget_description = __( 'Display the job\'s language', 'jobify' );
		$this->widget_id          = 'jobify_widget_job_language';
		$this->widget_name        = __( 'Jobify - Job: language', 'jobify' );
		$this->settings           = array(
			'title' => array(
				'type'  => 'text',
				'std'   => '',
				'label' => __( 'Title:', 'jobify' )
			)
		);

		//$this->settings = jobify_rcp_subscription_selector( $this->settings );

		parent::__construct();
	}

	/**
	 * widget function.
	 *
	 * @see WP_Widget
	 * @access public
	 * @param array $args
	 * @param array $instance
	 * @return void
	 */
	function widget( $args, $instance ) {
		if ( $this->get_cached_widget( $args ) )
			return;

		ob_start();

		extract( $args );

		global $post;

		$title = apply_filters( 'widget_title', isset ( $instance[ 'title' ] ) ? $instance[ 'title' ] : 'Ngôn ngữ trình bày hồ sơ', $instance, $this->id_base );
		$language = get_the_terms( $post->ID, 'job_listing_category' );

		if ( ! $language )
			return;

		echo $before_widget;
		?>

		<?php if ( $title ) echo $before_title . $title . $after_title; ?>

		<div class="job_listing-language">

			<?php the_language(); ?>

		</div>

		<?php
		echo $after_widget;

		$content = apply_filters( 'jobify_widget_job_language', ob_get_clean(), $instance, $args );

		echo $content;

		$this->cache_widget( $args, $content );
	}
}
// Register and load the widget
function wpb_load_job_language_widget() {
	register_widget( 'Jobify_Widget_Job_Language' );
}
add_action( 'widgets_init', 'wpb_load_job_language_widget' );

/**
 * Job: Deadline
 *
 * @since Jobify 1.6.0
 */
class Jobify_Widget_Job_Deadline extends Jobify_Widget {

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->widget_cssclass    = 'jobify_widget_job_deadline';
		$this->widget_description = __( 'Display the job\'s deadline', 'jobify' );
		$this->widget_id          = 'jobify_widget_job_deadline';
		$this->widget_name        = __( 'Jobify - Job: deadline', 'jobify' );
		$this->settings           = array(
			'title' => array(
				'type'  => 'text',
				'std'   => '',
				'label' => __( 'Title:', 'jobify' )
			)
		);

		//$this->settings = jobify_rcp_subscription_selector( $this->settings );

		parent::__construct();
	}

	/**
	 * widget function.
	 *
	 * @see WP_Widget
	 * @access public
	 * @param array $args
	 * @param array $instance
	 * @return void
	 */
	function widget( $args, $instance ) {
		if ( $this->get_cached_widget( $args ) )
			return;

		ob_start();

		extract( $args );

		global $post;

		$title = apply_filters( 'widget_title', isset ( $instance[ 'title' ] ) ? $instance[ 'title' ] : 'Hạn nộp hồ sơ', $instance, $this->id_base );
		$deadline = get_the_terms( $post->ID, 'job_listing_category' );

		if ( ! $deadline )
			return;

		echo $before_widget;
		?>

		<?php if ( $title ) echo $before_title . $title . $after_title; ?>

		<div class="job_listing-deadline">

			<?php the_deadline(); ?>

		</div>

		<?php
		echo $after_widget;

		$content = apply_filters( 'jobify_widget_job_deadline', ob_get_clean(), $instance, $args );

		echo $content;

		$this->cache_widget( $args, $content );
	}
}
// Register and load the widget
function wpb_load_job_deadline_widget() {
	register_widget( 'Jobify_Widget_Job_Deadline' );
}
add_action( 'widgets_init', 'wpb_load_job_deadline_widget' );

/**
 * Job: Times
 *
 * @since Jobify 1.6.0
 */
class Jobify_Widget_Job_Times extends Jobify_Widget {

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->widget_cssclass    = 'jobify_widget_job_times';
		$this->widget_description = __( 'Display the job\'s times', 'jobify' );
		$this->widget_id          = 'jobify_widget_job_times';
		$this->widget_name        = __( 'Jobify - Job: times', 'jobify' );
		$this->settings           = array(
			'title' => array(
				'type'  => 'text',
				'std'   => '',
				'label' => __( 'Title:', 'jobify' )
			)
		);

		//$this->settings = jobify_rcp_subscription_selector( $this->settings );

		parent::__construct();
	}

	/**
	 * widget function.
	 *
	 * @see WP_Widget
	 * @access public
	 * @param array $args
	 * @param array $instance
	 * @return void
	 */
	function widget( $args, $instance ) {
		if ( $this->get_cached_widget( $args ) )
			return;

		ob_start();

		extract( $args );

		global $post;

		$title = apply_filters( 'widget_title', isset ( $instance[ 'title' ] ) ? $instance[ 'title' ] : 'Hình thức làm việc', $instance, $this->id_base );
		$times = get_the_terms( $post->ID, 'job_listing_category' );

		if ( ! $times )
			return;

		echo $before_widget;
		?>

		<?php if ( $title ) echo $before_title . $title . $after_title; ?>

		<div class="job_listing-times">

			<?php the_jobtimes(); ?>

		</div>

		<?php
		echo $after_widget;

		$content = apply_filters( 'jobify_widget_job_times', ob_get_clean(), $instance, $args );

		echo $content;

		$this->cache_widget( $args, $content );
	}
}
// Register and load the widget
function wpb_load_job_times_widget() {
	register_widget( 'Jobify_Widget_Job_Times' );
}
add_action( 'widgets_init', 'wpb_load_job_times_widget' );

/**
 * Job: Categories
 *
 * @since Jobify 1.6.0
 */
class Jobify_Widget_Job_Categories extends Jobify_Widget {

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->widget_cssclass    = 'jobify_widget_job_categories';
		$this->widget_description = __( 'Display the job\'s categories', 'jobify' );
		$this->widget_id          = 'jobify_widget_job_categories';
		$this->widget_name        = __( 'Jobify - Job: Categories', 'jobify' );
		$this->settings           = array(
			'title' => array(
				'type'  => 'text',
				'std'   => '',
				'label' => __( 'Title:', 'jobify' )
			)
		);

		//$this->settings = jobify_rcp_subscription_selector( $this->settings );

		parent::__construct();
	}

	/**
	 * widget function.
	 *
	 * @see WP_Widget
	 * @access public
	 * @param array $args
	 * @param array $instance
	 * @return void
	 */
	function widget( $args, $instance ) {
		if ( $this->get_cached_widget( $args ) )
			return;

		ob_start();

		extract( $args );

		global $post;

		if ( ! get_option( 'job_manager_enable_categories' ) )
			return;

		$title = apply_filters( 'widget_title', isset ( $instance[ 'title' ] ) ? $instance[ 'title' ] : 'Ngành nghề', $instance, $this->id_base );
		$categories = get_the_terms( $post->ID, 'job_listing_category' );

		if ( ! $categories )
			return;

		echo $before_widget;
		?>

		<?php if ( $title ) echo $before_title . $title . $after_title; ?>

		<div class="job_listing-categories">

			<?php foreach ( $categories as $category ) : ?>

				<?php if ( class_exists( 'WP_Job_Manager_Cat_Colors' ) ) : ?>

					<div><a href="<?php echo get_term_link( $category, 'job_listing_category' ); ?>" class="job-category <?php echo $category->slug; ?>"><?php echo $category->name; ?></a></div>

				<?php else : ?>

					<div><a href="<?php echo get_term_link( $category, 'job_listing_category' ); ?>"><i class="icon-book-open"></i> <?php echo $category->name; ?></a></div>

				<?php endif; ?>

			<?php endforeach; ?>

		</div>

		<?php
		echo $after_widget;

		$content = apply_filters( 'jobify_widget_job_categories', ob_get_clean(), $instance, $args );

		echo $content;

		$this->cache_widget( $args, $content );
	}
}
// Register and load the widget
function wpb_load_job_categories_widget() {
	register_widget( 'Jobify_Widget_Job_Categories' );
}
add_action( 'widgets_init', 'wpb_load_job_categories_widget' );

/**
 * Job/Resume: Logo
 *
 * @since Jobify 1.6.0
 */
class Jobify_Widget_Job_Company_Logo extends Jobify_Widget {

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->widget_cssclass    = 'jobify_widget_job_company_logo';
		$this->widget_description = __( 'Display the company logo or resume picture', 'jobify' );
		$this->widget_id          = 'jobify_widget_job_company_logo';
		$this->widget_name        = __( 'Jobify - Job/Resume: Logo', 'jobify' );
		$this->settings           = array(
			'title' => array(
				'type'  => 'text',
				'std'   => '',
				'label' => __( 'Title:', 'jobify' )
			)
		);

		//$this->settings = jobify_rcp_subscription_selector( $this->settings );

		parent::__construct();
	}

	/**
	 * widget function.
	 *
	 * @see WP_Widget
	 * @access public
	 * @param array $args
	 * @param array $instance
	 * @return void
	 */
	function widget( $args, $instance ) {
		if ( $this->get_cached_widget( $args ) )
			return;

		global $wp_embed;

		ob_start();

		extract( $args );

		global $post;

		$title = apply_filters( 'widget_title', isset ( $instance[ 'title' ] ) ? $instance[ 'title' ] : '', $instance, $this->id_base );

		echo $before_widget;
		?>

		<?php if ( $title ) echo $before_title . $title . $after_title; ?>

		<?php if ( 'job_listing' == get_post_type() ) : ?>

			<?php
				if ( class_exists( 'Astoundify_Job_Manager_Companies' ) && '' != get_the_company_name() ) :
					$companies   = Astoundify_Job_Manager_Companies::instance();
					$company_url = esc_url( $companies->company_url( get_the_company_name() ) );
			?>
			<a href="<?php echo $company_url; ?>" target="_blank"><?php the_company_logo(); ?></a>
			<?php else : ?>
				<?php the_company_logo(); ?>
			<?php endif; ?>

		<?php else : ?>

			<?php the_candidate_photo( 'large' ); ?>

		<?php endif; ?>

		<?php
		echo $after_widget;

		$content = apply_filters( 'jobify_widget_job_company_logo', ob_get_clean(), $instance, $args );

		echo $content;

		$this->cache_widget( $args, $content );
	}
}
// Register and load the widget
function wpb_load_job_company_logo() {
	register_widget( 'Jobify_Widget_Job_Company_Logo' );
}
add_action( 'widgets_init', 'wpb_load_job_company_logo' );