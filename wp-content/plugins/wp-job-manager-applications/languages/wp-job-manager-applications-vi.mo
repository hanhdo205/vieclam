��            )   �      �     �     �     �  7   �          #     0     7     D  	   U     _     c     p     ~     �     �     �     �  /   �  	   �  %   �  &      =   G  .   �     �     �     �     �       �  2     �     �       V   -  	   �     �     �     �     �     �     �  -   �     	     +	     I	     Z	     p	     �	  L   �	     �	  *   �	  +   "
  N   N
  =   �
  
   �
     �
     �
                                                                	      
                                                                             Application Message Application status Applied Are you sure you want to delete this? There is no undo. Choose an online resume... Date Applied Delete Download CSV Filter by status Full name Job Newest first Online Resume Rating (out of 5) Save Changes Sort by name Sort by rating Status The job applications for "%s" are listed below. Upload CV You have already applied for this job You have already applied for this job. You must <a href="%s">sign in</a> to apply for this position. Your cover letter/message sent to the employer job_applicationArchived job_applicationHired job_applicationInterviewed job_applicationNew job_applicationOffer extended Project-Id-Version: WP Job Manager - Applications v1.4.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-08-11 11:54+0700
PO-Revision-Date: 2015-08-11 12:24+0700
Last-Translator: hanhdo <hanhdo205@gmail.com>
Language-Team: mycareer.vn <hanhdo205@gmail.com>
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.5.5
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: .
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: ..
 Thư giới thiệu Phân loại hồ sơ này Đã nộp hồ sơ Bạn có chắc muốn xoá? Bạn không thể phục hồi lại sau khi đã xoá. Chọn... Ngày Xoá Tải về dạng CSV Xem theo tình trạng hồ sơ Họ và tên Công việc Hiển thị hồ sơ mới lên đầu tiên Hồ sơ sẵn có Đánh giá (thang điểm 5) Lưu thay đổi Sắp xếp theo tên Sắp xếp theo đánh giá Tình trạng Danh sách ứng tuyển cho vị trí "%s" được liệt kê bên dưới Đính kèm CV Bạn đã ứng tuyển công việc này Bạn đã ứng tuyển công việc này. Bạn cần phải <a href="%s">đăng nhập</a> trước khi nộp hồ sơ. Thư tự giới thiệu/nội dung gởi nhà tuyển dụng Bảo lưu Đã tuyển Đã phỏng vấn Ứng viên mới Vào vòng trong 