<?php
/**
 * Single view Company information box
 *
 * Hooked into single_job_listing_start priority 30
 *
 * @since  1.14.0
 */

if ( ! get_the_company_name() ) {
	return;
}
?>
<div class="company" itemscope itemtype="http://data-vocabulary.org/Organization">
	<?php the_company_logo(); ?>

	<p class="name">
		<?php if ( $website = get_the_company_website() ) : ?>
			<a class="website" href="<?php echo esc_url( $website ); ?>" itemprop="url" target="_blank" rel="nofollow"><?php _e( 'Website', 'wp-job-manager' ); ?></a>
		<?php endif; ?>
		<?php //the_company_twitter(); ?>
		<?php the_company_name( '<strong itemprop="name">', '</strong>' ); ?>
		<p class="location" itemprop="jobLocation" style="margin:0 0 0 3em;padding:0 0 0 1em; line-height:1.5em;"><?php the_job_address(); ?></p>
		<p class="contact" itemprop="jobLocation" style="margin:0 0 0 3em;padding:0 0 0 1em; line-height:1.5em;"><?php _e( 'Contact person: ', 'wp-job-manager' ); ?><?php the_contact_name(); ?></p>
	</p>
	<?php the_company_tagline( '<p class="tagline">', '</p>' ); ?>
	<?php //the_company_video(); ?>
</div>