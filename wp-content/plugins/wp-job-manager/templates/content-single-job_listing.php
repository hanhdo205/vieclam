<?php global $post; ?>
<div class="single_job_listing" itemscope itemtype="http://schema.org/JobPosting">
	<meta itemprop="title" content="<?php echo esc_attr( $post->post_title ); ?>" />

	<?php if ( get_option( 'job_manager_hide_expired_content', 1 ) && 'expired' === $post->post_status ) : ?>
		<div class="job-manager-info"><?php _e( 'This listing has expired.', 'wp-job-manager' ); ?></div>
	<?php else : ?>
		<?php
			/**
			 * single_job_listing_start hook
			 *
			 * @hooked job_listing_meta_display - 20
			 * @hooked job_listing_company_display - 30
			 */
			do_action( 'single_job_listing_start' );
		?>
		<div style="border-top:1px solid #ebebeb;"></div>
		<div class="single-job main col-lg-9 col-md-8">
		<div class="job_description" itemprop="description">
			<?php display_job_tags_data(); ?>
			<h3>Nội dung tuyển dụng</h3>
			<?php echo apply_filters( 'the_job_description', get_the_content() ); //setPostViews(get_the_ID());?>
		</div>
		<div class="job_contact_fields">
			<?php
			/**
			 * single_job_listing_info_end hook
			 *
			 * @hooked job_listing_meta_display - 20
			 * @hooked job_listing_company_display - 30
			 */
			do_action( 'single_job_listing_info_end' );
		?>
		</div>

		<?php //if ( candidates_can_apply() ) : ?>
			<?php //get_job_manager_template( 'job-application.php' ); ?>
		<?php //endif; ?>

		<?php
			/**
			 * single_job_listing_end hook
			 */
			do_action( 'single_job_listing_end' );
		?>
		</div>
		<aside class="col-lg-3 col-md-4 kad-sidebar summary-job-details sumary_box" role="complementary">
              <div class="single-job-sidebar sidebar">

              <h3>Thông tin tuyển dụng</h3>
			  <?php the_widget( 'Jobify_Widget_Job_Level', array(), $args ); ?>
			  <?php the_widget( 'Jobify_Widget_Job_Categories', array(), $args ); ?>
			  <?php the_widget( 'Jobify_Widget_Job_Times', array(), $args ); ?>
			  <?php the_widget( 'Jobify_Widget_Job_Language', array(), $args ); ?>
			  <?php the_widget( 'Jobify_Widget_Job_Deadline', array(), $args ); ?>

			  <?php

			  if ( function_exists( 'sharing_display' ) ) {
				    sharing_display( '', true );
				}
				 
				if ( class_exists( 'Jetpack_Likes' ) ) {
				    $custom_likes = new Jetpack_Likes;
				    echo $custom_likes->post_likes( '' );
				}

			  ?>

			  <?php
			  do_action( 'bookmark_at_sidebar' );
			  ?>

              </div><!-- /.sidebar -->
        </aside>
	<?php endif; ?>
</div>