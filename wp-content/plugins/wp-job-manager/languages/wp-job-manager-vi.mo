��    c      4  �   L      p     q  :   x  
   �     �     �      �  #   	     *	  	   J	  S   T	  6   �	     �	  !   �	  
   
     
     )
     :
     B
     Y
     r
     �
     �
  
   �
  -   �
  2   �
  #     
   2     =  z   W     �     �     �     �               3     I     V  &   b     �     �     �     �  &   �     �  	          	        !     2     :  6   P     �  Z   �     �     �     �       	              -     6     ;     C  1   L     ~     �     �     �     �     �     �     �     �     
     "     +     2     @     M     Z     j     s     �     �  '   �     �     �     �     �     �  3   �       
   *  i   5     �  
   �     �  �  �     �  L   �       *     I   A  4   �  *   �  /   �       U   )  G        �  *   �               1     I     U     r     �     �     �  
   �  5   �  <   �  #   :     ^     l  �   �     V     j     x     �     �  !   �  )   �     �               0     5     O  &   U  ,   |     �     �     �     �     �       $     A   A     �  _   �     �     �  &   	     0     >     W     e       
   �  
   �  M   �     �  	             %     2     9     F     ]  "   z     �  	   �     �     �     �     �            &   &     M     T  .   b     �     �     �     �     �  B   �          $  �   7     �     �  $   �         6          >   9   I   .          &   G   (   F           S                 )      =   7   ^              0   O   V       ;   W         '       P   *       ?   -   H          [      +      N   :       b   5   B          L   /   R   2   ,                 3          	   T   Z   Y   %   A              
                      !       $   `   1   @          "   <      U           c   J              \      a                  D   X       M      E   _           4                    K   Q      8   C          #   ]    %s ago %s draft updated. <a target="_blank" href="%s">Preview</a> %s expired %s has been deleted %s has been filled %s has been marked as not filled %s published. <a href="%s">View</a> %s restored to revision from %s %s saved. %s scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview</a> %s submitted. <a target="_blank" href="%s">Preview</a> %s updated. %s updated. <a href="%s">View</a> (optional) Account Creation Account Required Add-ons Allow account creation Application Email or URL Application email Application email/URL Approve Approve %s Are you sure you want to delete this listing? Automatically Generate Username from Email Address Brief description about the company Categories Choose a category&hellip; Choose whether to enable categories. Categories must be setup by an admin to allow users to choose them during submission. Company Details Company Name Company Tagline Company name Contact person:  Custom field deleted. Custom field updated. Date Expires Date Posted Default page title (wizard)Post a Job Delete Description Edit Enable categories for listings Enable category multiselect by default Enter the name of the company Expire %s Expires Featured? Filled Positions Filled? Hide filled positions How many listings should be shown per page by default? ID: %d If enabled, the category select box will default to a multiselect on the [jobs] shortcode. Job Job Listings Job Location Job Submission Job Title Job category Job type Jobs Keyword Keywords Leave this blank if the location is not important Listings Per Page Load more listings Load previous listings Location M j, Y M j, Y @ G:i Mark filled Mark not filled Modify this job search Multi-select Categories Position Posted Posted %s ago Save Changes Save changes Select category Settings Showing all %s Sign out Status Submitting listings requires an account Tagline Title Type View WP Job Manager Add-ons You are currently signed in as <strong>%s</strong>. Your account Your email Your listings are shown in the table below. Expired listings will be automatically removed after 30 days. by %s by a guest e.g. "London" Project-Id-Version: WP Job Manager
Report-Msgid-Bugs-To: http://wordpress.org/tag/WP-Job-Manager
POT-Creation-Date: 2015-08-09 22:09+0700
PO-Revision-Date: 2015-08-14 14:16+0700
Last-Translator: hanhdo <hanhdo205@gmail.com>
Language-Team: Vietnamese (http://www.transifex.com/projects/p/wp-job-manager/language/vi/) <hanhdo205@gmail.com>
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 1.5.5
X-Poedit-Basepath: .
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: ..
 %s trước %s bản nháp đã cập nhật. <a target="_blank" href="%s">Xem thử</a> %s đã hết hạn Tin tuyển dụng "%s" đã được xoá Tin tuyển dụng "%s" đã được đánh dấu ngưng nhận hồ sơ Tin tuyển dụng "%s" tiếp tục nhận hồ sơ %s đã xuất bản. <a href="%s">Xem</a> %s đã khôi phục lại từ phiên bản %s %s đã lưu. %s scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Xem thử</a> %s đã được đưa lên. <a target="_blank" href="%s">Xem thử</a> %s đã cập nhật. %s đã cập nhật. <a href="%s">Xem</a> (không bắt buộc) Tạo tài khoản Yêu cầu tài khoản Tiện ích Cho phép tạo tài khoản Email liên hệ hoặc URL Email liên hệ Email liên hệ/URL Duyệt Duyệt %s Bạn có chắc muốn xoá tin tuyển dụng này? Tự động tạo tên đăng nhập từ thư điện tử Mô tả ngắn gọn về Công ty Chuyên mục Chọn ngành nghề &hellip; Choose whether to enable categories. Hạng mục cần được cấu hình với quản trị viên để có thể cho phép người dùng chọn mục đó trong quá trình đăng tải thông tin. Thông tin Công ty Tên Công ty Qui mô Công ty Tên Công ty Người liên hệ:  Trường tùy chỉnh đã xóa. Trường tùy chỉnh đã cập nhật. Hiển thị đến Ngày đăng Đăng tuyển dụng Xóa Nội dung tuyển dụng Sửa Kích hoạt các mục cho danh sách Mặc định cho phép chọn nhiều mục Nhập tên Công ty Hết hạn %s Hết hạn Nổi bật? Vị trí đã đăng ký hêt Đã đăng ký đủ? Ẩn vị trí đã đăng ký đủ Mặc định có bao nhiêu danh sách hiển thị mối trang? Mã: %d Nếu cho phép, the category select box will default to a multiselect on the [jobs] shortcode. việc làm Danh sách việc Nơi làm việc (tỉnh/thành phố) Đăng việc Tiêu đề công việc Ngành nghề Phân loại công việc Việc làm Từ khóa Từ khóa Bạn có thể để trống ô này nếu cảm thấy không cần thiết Danh sách mỗi trang Xem thêm Kết quả trước Địa chỉ M j, Y M j, Y @ G:i Ngưng nhận hồ sơ Tiếp tục nhận hồ sơ Bộ lọc kết quả tìm việc Chọn nhiều mục Vị trí Đã đăng Đăng tuyển %s trước Lưu thay đổi Lưu thay đổi Chọn chuyên mục Cấu hình Kết quả tìm kiếm việc làm %s Thoát Trạng thái Danh sách đăng lên yêu cầu tài khoản Qui mô Công ty Tiêu đề Kiểu Xem WP Job Manager Tiện Ích Bạn đang đăng nhập bằng tài khoản <strong>%s</strong>. Tài khoản Địa chỉ email Tin tuyển dụng của Quý Công ty được liệt kê bên dưới. Các tin hết hạn hiển thị sẽ được xoá sau 30 ngày. bởi %s bởi khách 21 Nguyễn Trãi, Quận 1, Tp. HCM 