��    I      d  a   �      0     1     :     N     a     w  
   �      �     �     �  
   �     �     �     �  ,   �  *   ,     W  (   `     �     �     �     �  	   �  
   �     �     �  
   �     �     �     �  	                  $     3     8     >  L   L  H   �     �     �     �     �     	     %	  	   -	     7	     H	     Z	     j	     y	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  `   �	     \
     c
  3   r
  +   �
     �
  
   �
     �
  	   �
  j     ,   n     �  �  �     o      {      �  "   �     �     �  M        Z     x     �  	   �     �     �  1   �          .  Q   <     �     �     �     �     �     �     �     �                     "     '     >     Y     m     �     �     �  t   �  Q   (     z     �     �     �     �     �     �     �     �               -     B     P     e  	   v     �     �     �     �     �     �  e   �     H     L  B   e  0   �     �     �     �       6   "     Y     h     -   ?   	               0      5      =   (      G   +       <   C         $               &   2   6   @      H          A   /   4       #                7                  F   
                   '   ;      :       .         9   3      >                                      ,       E   8       )      D   B          "   1       I   %          !      *    %s at %s %s has been deleted %s has been hidden %s has been published &larr; Edit resume (optional) A link to a video about yourself Add Education Add Experience Add Resume Add URL All Resumes Any Location Are you sure you want to delete this resume? Are you sure you want to remove this item? Category Comma separate a list of relevant skills Contact Contact using webmail:  Delete Edit Education Education: Employer Employer: %s Experience Experience: Hidden Hide Job Title Job Title: %s Location Maximum of %d. Name Notes Online resume Optionally provide links to any of your websites or social network profiles. Optionally upload your resume for employers to view. Max. file size: %s. Photo Preview Preview &rarr; Professional Title Professional title Publish Published Qualification(s) Qualification: %s Resume Category Resume Content Resume File Resume category Resume file Save changes School name Sign out Skills Start/end date Status Submit Resume &rarr; Title To contact this candidate email <a class="job_application_email" href="mailto:%1$s%2$s">%1$s</a> URL(s) Updated %s ago You are currently signed in as <strong>%s</strong>. You do not have any active resume listings. Your account Your email Your full name Your name Your resume can be viewed, edited or removed below. Your resume(s) can be viewed, edited or removed below. e.g. "London, UK", "New York", "Houston, TX" e.g. "Web Developer" Project-Id-Version: WP Job Manager - Resume Manager 1.11.1
Report-Msgid-Bugs-To: http://wordpress.org/support/plugin/wp-job-manager-resumes
POT-Creation-Date: 2015-08-14 10:15+0700
PO-Revision-Date: 2015-08-21 09:44+0700
Last-Translator: hanhdo <hanhdo205@gmail.com>
Language-Team: mycareer.vn <hanhdo205@gmail.com>
Language: vi_VN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.5
Plural-Forms: nplurals=1; plural=0;
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: ..
 %s tại %s Hồ sơ "%s" đã được xoá Hồ sơ "%s" đã được ẩn Hồ sơ "%s" đã được đăng &larr; Chỉnh sửa  (không bắt buộc) Nếu bạn có clip tự giới thiệu, vui lòng ghi rõ link (Youtube...) Thêm Quá trình học tập Thêm Kinh nghiệm làm việc Tạo mới Thêm URL Tất cả ứng viên Tất cả tỉnh/thành Bạn có chắc mình muốn xoá hồ sơ này? Bạn muốn bỏ mục này? Ngành nghề Các ký năng sở trường của bạn, phân cách bằng dấu phẩy (,)... Liên hệ Ứng viên này Liên hệ bằng webmail:  Xoá Sửa Học vấn Học vấn: Công ty Công ty: %s Kinh nghiệm Kinh nghiệm: Ẩn Ẩn Vị trí, chức vụ Vị trí, chức vụ: %s Tỉnh/thành phố Tối đa %d kỹ năng. Tên Thông tin bổ sung Hồ sơ sẵn có Bạn có thể cung cấp các link đến các blog, tài khoản mạng xã hội hoặc các website của bạn Bạn có thể đính kèm hồ sơ (word, excel, hình ảnh...). Tối đa %s. Ảnh của bạn Xem trước Xem thử &rarr; Tiêu đề hồ sơ Tiêu đề hồ sơ Đăng Đăng Chuyên ngành Chuyên ngành: %s Ngành nghề Giới thiệu bản thân Hồ sơ đính kèm Ngành nghề Hồ sơ đính kèm Lưu thay đổi Trường Thoát Kỹ năng, sở trường Từ ngày - đến ngày Trạng thái Lưu &rarr; Tiêu đề Địa chỉ email của ứng viên <a class="job_application_email" href="mailto:%1$s%2$s">%1$s</a> URL Cập nhật %s trước Bạn đang đăng nhập bằng tài khoản <strong>%s</strong>. Bạn chưa có hồ sơ nào. Hãy tạo mới! Tài khoản Địa chỉ email Họ và tên đầy đủ Họ và tên Bạn có thể xem, sửa, xoá hồ sơ của mình. vd "Hà Nội" vd "Web Developer" 