��    .      �  =   �      �     �             '   *  	   R  
   \     g     t  
   �  +   �  
   �     �     �     �     �     �     �     �          
          .     ?  	   K     U     a     o  
   �     �  	   �  
   �     �     �     �     �  \   �  .   S     �  
   �     �     �     �  %   �     �  M     �  Q  <   
  #   O
  +   s
  C   �
     �
     �
          %     =  C   W     �     �     �     �     �     �     �     �            M        h     u     �     �     �  @   �     �          /     K  K   \  
   �  
   �  &   �  �   �  )   f  &   �     �  4   �     
       B   (     k  |   |                                     *                      &         +                          '          ,          (          -       !   .   
          "   	       #         $      )             %                 %s has been %s %s has been deleted %s has been triggered <i class="icon-envelope"></i> Add alert Add alert Alert Name Any category Any job type Any region Are you sure you want to delete this alert? Categories Daily Date Created Delete Disable Disabled Edit Email Frequency Enable Enabled Enter a name for your alert Enter your email Fortnightly Frequency Hide notice Invalid Alert Job Alert Results Matching "%s" Job Alerts Job Type Job Type: Job region Jobs matching your "%s" alert: Keyword Keywords No jobs found No jobs were found matching your search. Login to your account to change your alert criteria Optionally add a keyword to match jobs against Please name your alert Save alert Send alerts with matches only Show Results Status The date this alert will next be sent Weekly Your job alerts are shown in the table below. Your alerts will be sent to %s. Project-Id-Version: WP Job Manager - Job Alerts 1.1.0
Report-Msgid-Bugs-To: http://wordpress.org/tag/wp-job-manager-alerts
POT-Creation-Date: 2015-08-09 18:59+0700
PO-Revision-Date: 2015-08-11 08:50+0700
Last-Translator: hanhdo <hanhdo205@gmail.com>
Language-Team: LANGUAGE <hanhdo205@gmail.com>
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Generator: Poedit 1.5.5
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: .
Plural-Forms: nplurals=1; plural=0;
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: ..
 Trạng thái của thông báo "%s" đã được đặt %s Thông báo "%s" đã được xoá Thông báo "%s" đã được kích hoạt <i class="icon-envelope"></i> Nhận email việc làm tương tự Tạo thông báo mới Tên gợi nhớ Tất cả ngành nghề Tất cả hình thức Tất cả địa điểm Bạn chắc chắn muốn xoá thông báo việc làm này chứ? Ngành nghề 1 ngày 1 lần Ngày đăng ký Xoá Ngưng Tạm ngưng Sửa Gởi email cho tôi mỗi Chạy Hoạt động Chọn 1 tên gợi nhớ cho mỗi lần đăng ký nhận việc làm mới Nhập email 2 tuần 1 lần Tần suất Ẩn cảnh báo Không phù hợp Kết quả phù hợp với thông báo việc làm mới "%s"  Thông báo việc làm mới Phân loại công việc Phân loại công việc:  Nơi làm việc Các công việc phù hợp với thông báo việc làm "%s" của bạn Từ khoá Từ khoá Không có việc làm nào phù hợp Không có công việc nào phù hợp yêu cầu của bạn. Bạn nên đăng nhập và thay đổi lại 1 vài yêu cầu Từ khoá liên quan đến công việc Bạn phải chọn 1 tên gợi nhớ Đăng ký nhận thông báo Chỉ gởi cho tôi những công việc phù hợp Xem việc làm Trạng thái Thông báo này sẽ được gởi tiếp lần tới vào ngày 1 tuần 1 lần Các thông báo việc làm mới của bạn được liệt kê ở dưới đồng thời được gởi tới email %s. 